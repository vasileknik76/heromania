CREATE TABLE level_guess (
    id integer primary key AUTOINCREMENT,
    pic varchar(255) not null,
    answer varchar(32) not null
);

CREATE TABLE part_to_level_guess (
    partNumber integer not null,
    levelId integer not null
);

CREATE TABLE quiz_question(
    id integer primary key AUTOINCREMENT,
    name varchar(512) NOT NULL
);

CREATE TABLE quiz_answer_right (
	name varchar(256),
	quiz_question_id integer NOT NULL
);

CREATE TABLE quiz_answer_wrong (
	name varchar(256),
	quiz_question_id integer NOT NULL
);

CREATE TABLE level_guess_save (
    levelId integer,
    letters varchar(64),
    fields varchar(64),
    hintMask varchar(64),
    isHalfWordUsed integer
);

CREATE TABLE level_guess_completed (
    levelId integer
);

INSERT into level_guess(pic, answer) VALUES
("the_flash.png", "флэш"),
("captain_america.png", "капитан америка"),
("hulk.png", "халк"),
("shazam.png", "шазам"),
("pied_piper.png", "крысолов"),
("aquaman.png", "аквамен"),
("green_arrow.png", "зеленая стрела"),
("the_atom.png", "атом"),
("green_lantern.png", "зеленый фонарь"),
("deadpool.png", "дэдпул"),
("wonder_woman.png", "чудо женщина"),
("magneto.png", "магнето"),
("the_thing.png", "существо"),
("the_vision.png", "вижн"),
("darkseid.png", "дарксайд"),
("superman.png", "супермен"),
("ghost_rider.png", "призрачный гонщик"),
("beast.png", "зверь"),
("ares_marvel.png", "арес"),
("iron_man.png", "железный человек"),
("antman.png","человек-муравей"),
("colossus.png","колосс"),
("black_panther.png","черная пантера"),
("thor.png","тор"),
("falcon.png","сокол"),
("scarlet_witch.png","алая ведьма"),
("booster_gold.png","золотой ускоритель"),
("raven.png","рэйвен"),
("martian_manhunter.png","марсианский охотник"),
("supergirl.png","супергерл"),
("quicksilver.png","ртуть"),
("black_widow.png","черная вдова"),
("sinestro.png","синестро"),
("static.png","статик"),
("bane.png","бэйн"),
("deathstroke.png","дефстроук"),
("apocalypse.png", "апокалипсис"),
("archangel.png", "архангел"),
("azazel.png", "азазель"),
("batgirl.png", "бэтгерл"),
("batman.png", "бэтман"),
("black_adam.png", "черный адам"),
("captain_cold.png", "капитан холод"),
("clayface.png", "глиноликий"),
("cyborg.png", "киборг"),
("daredevil.png", "сорвиголова"),
("firestorm.png", "огненный шторм"),
("harley_quinn.png", "харли квинн"),
("human_torch.png", "человек-факел"),
("invisible_woman.png", "женщина-невидимка"),
("joker.png", "джокер"),
("killer_frost.png", "убийца мороз"),
("king_shark.png", "король акул"),
("mr_fantastic.png", "мистер фантастик"),
("professor_x.png", "профессор икс"),
("robin.png", "робин"),
("silver_surfer.png", "серебряный серфер"),
("spider_man.png", "человек-паук"),
("storm.png", "шторм"),
("ultron.png", "альтрон"),
("wolverine.png", "россомаха"),
("cyclops.png", "циклоп");


INSERT into part_to_level_guess(partNumber, levelId) VALUES
(1, 19),
(1, 3),
(1, 24),
(1, 36),
(1, 43),
(1, 33),
(1, 16),
(1, 7),
(1, 15),
(1, 38),
(1, 58),
(1, 40),
(1, 17),
(1, 55),
(1, 37),
(2, 35),
(2, 26),
(2, 2),
(2, 60),
(2, 47),
(2, 42),
(2, 1),
(2, 11),
(2, 27),
(2, 54),
(2, 51),
(2, 56),
(2, 5),
(2, 14),
(2, 53),
(3, 18),
(3, 21),
(3, 46),
(3, 20),
(3, 34),
(3, 10),
(3, 9),
(3, 25),
(3, 44),
(3, 6),
(3, 8),
(3, 4),
(3, 57),
(3, 52),
(3, 29),
(4, 50),
(4, 23),
(4, 13),
(4, 22),
(4, 41),
(4, 31),
(4, 12),
(4, 59),
(4, 39),
(4, 49),
(4, 45),
(4, 32),
(4, 30),
(4, 48),
(4, 28);

INSERT into quiz_question(name) VALUES ('Кто из этих персонажей не принимал участия в "Мстителях" ?');
INSERT into quiz_answer_right(name, quiz_question_id) VALUES ('Дэдпул ', 1);
INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Тор', 1);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Капитан Америка ', 1);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Соколиный глаз ', 1);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Вижн', 1);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Черная Вдова ', 1);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Железный человек ', 1);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Халк', 1);

INSERT into quiz_question(name) VALUES ('Какое оружие носил Тор ?');
INSERT into quiz_answer_right(name, quiz_question_id) VALUES ('Молот ', 2);
INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Лук ', 2);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Меч ', 2);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Был без оружия ', 2);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Дубинка ', 2);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Нож', 2);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Пистолет ', 2);

INSERT into quiz_question(name) VALUES ('Какой способностью обладал супермен ? ');
INSERT into quiz_answer_right(name, quiz_question_id) VALUES ('Полет и супер сила ', 3);
INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Телепортация ', 3);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Супер эластичность ', 3);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Невидимость ', 3);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Быстрая регенерация ', 3);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Управление погодой ', 3);

INSERT into quiz_question(name) VALUES ('Каким цветом был костюм у человека паука ? ');
INSERT into quiz_answer_right(name, quiz_question_id) VALUES ('Красный ', 4);
INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Розовый ', 4);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Белый ', 4);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Желтый ', 4);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Зеленый ', 4);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Фиолетовый ', 4);

INSERT into quiz_question(name) VALUES ('Какое оружие было у "Соколиного Глаза " ? ');
INSERT into quiz_answer_right(name, quiz_question_id) VALUES ('Лук и стрелы ', 5);
INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Чеснок и стрелы ', 5);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Автомат ', 5);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Снайперская винтовка ', 5);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Когти ', 5);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Мечи ', 5);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Арбалет ', 5);

INSERT into quiz_question(name) VALUES ('Какой персонаж из вселенной Марвел мог быстро перемещаться ? ');
INSERT into quiz_answer_right(name, quiz_question_id) VALUES ('Ртуть ', 6);
INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Флэш', 6);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Человек -паук ', 6);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Каратель ', 6);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Халк', 6);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Сокол ', 6);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Человек-муравей ', 6);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Шазам ', 6);

INSERT into quiz_question(name) VALUES ('Кто из вселенной DC имел такое же оружие как и у "Соколиного Глаза" ? ');
INSERT into quiz_answer_right(name, quiz_question_id) VALUES ('Зеленая стрела ', 7);
INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Зеленая пуля ', 7);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Архангел ', 7);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Бэтмен ', 7);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Дэдшот ', 7);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Дефстроук ', 7);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Не один из этих персонажей ', 7);

INSERT into quiz_question(name) VALUES ('Какой была эмблема у флэша ');
INSERT into quiz_answer_right(name, quiz_question_id) VALUES ('Молния ', 8);
INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Буква "F" ', 8);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Не было эмблемы ', 8);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Бегущий человек ', 8);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Пантера ', 8);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Паук ', 8);

INSERT into quiz_question(name) VALUES ('Какого цвета был костюм у Халка ? ');
INSERT into quiz_answer_right(name, quiz_question_id) VALUES ('У него не было костюма ', 9);
INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Синий ', 9);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Красный ', 9);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Черный ', 9);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Белый ', 9);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Сине-черный ', 9);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Бело-черный ', 9);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Серый ', 9);

INSERT into quiz_question(name) VALUES ('Кто из этих персонажей силой мысли мог создавать предметы ?');
INSERT into quiz_answer_right(name, quiz_question_id) VALUES ('Зеленый фонарь ', 10);
INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Импульс ', 10);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Шазам ', 10);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Ртуть ', 10);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Локи ', 10);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Веном ', 10);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Росомаха ', 10);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Капитан холод ', 10);

INSERT into quiz_question(name) VALUES ('Кто из этих героев состоит во вселенной DC');
INSERT into quiz_answer_right(name, quiz_question_id) VALUES ('Дэдшот ', 11);
INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Человек- паук ', 11);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Алая ведьма ', 11);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Ртуть ', 11);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Черная пантера ', 11);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Черная вдова ', 11);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Альтрон', 11);

INSERT into quiz_question(name) VALUES ('В каком городе жил флэш ');
INSERT into quiz_answer_right(name, quiz_question_id) VALUES ('Централ Сити ', 12);
INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Стар Сити ', 12);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Калифорния ', 12);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Нью Йорк ', 12);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Не один из этих ', 12);

INSERT into quiz_question(name) VALUES ('Как зовут Капитана Америку ');
INSERT into quiz_answer_right(name, quiz_question_id) VALUES ('Стивен Роджерс ', 13);
INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Оливер Квин', 13);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Уолли Уэст ', 13);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Ник Фьюре ', 13);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Мартин Гудмен ', 13);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Кларк Кент ', 13);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Джери Старфаер ', 13);

INSERT into quiz_question(name) VALUES ('Кто из  этих героев был слепым ');
INSERT into quiz_answer_right(name, quiz_question_id) VALUES ('Сорвиголова', 14);
INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Дэдпул ', 14);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Колосс', 14);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Капитан Марвел ', 14);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Черный Адам ', 14);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Статик ', 14);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Зеленая стрела ', 14);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Траектория ', 14);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Профессор Икс ', 14);

INSERT into quiz_question(name) VALUES ('Каким оружием обладал Блэйд ');
INSERT into quiz_answer_right(name, quiz_question_id) VALUES ('Катана ', 15);
INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Ножи ', 15);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Бластер ', 15);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Хлыст ', 15);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Щит ', 15);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Трезубец ', 15);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Лук и стрелы ', 15);

INSERT into quiz_answer_wrong(name, quiz_question_id) VALUES ('Дубинки ', 15)