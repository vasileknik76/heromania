package ru.mintscale.heromania;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;

public class BoostButton extends ImageButton implements View.OnClickListener {
	private static final String LOG_TAG = "BoostButton";
	private Drawable backgroundPressed;
	private Drawable backgroundNormal;
	private int counterBackgroundColor = Color.RED;
	private int counterFontColor = Color.RED;
	private boolean mIsPressed = false;
	private float counterFontSize = 4;

	private int counter;
	private Drawable img;

	private TextPaint mTextPaint;

	public BoostButton(Context context) {
		super(context);
		init(null, 0);
	}

	public BoostButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(attrs, 0);
	}

	public BoostButton(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(attrs, defStyle);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if(event.getAction() == MotionEvent.ACTION_UP){
			return performClick();
		}
		return true;
	}

	//Фон только начиная с Jeally Bean(16)
	private void init(AttributeSet attrs, int defStyle) {
		// Load attributes
		final TypedArray a = getContext().obtainStyledAttributes(
				attrs, R.styleable.BoostButton, defStyle, 0);

		setCounter(a.getInt(
				R.styleable.BoostButton_counter, 0));

		setCounterBackgroundColor(a.getColor(R.styleable.BoostButton_counterBackgroundColor, getCounterBackgroundColor()));
		setBackgroundPressed(a.getDrawable(R.styleable.BoostButton_backgroundPressed));
		setBackgroundNormal(a.getDrawable(R.styleable.BoostButton_backgroundNormal));
		setCounterFontColor(a.getColor(R.styleable.BoostButton_counterFontColor, getCounterFontColor()));

		setCounterFontSize(a.getDimension(R.styleable.BoostButton_counterFontSize, getCounterFontSize()));

		if (a.hasValue(R.styleable.BoostButton_img)) {
			setImg(a.getDrawable(R.styleable.BoostButton_img));
		}

		a.recycle();

		setClickable(true);
		setOnClickListener(this);

		// Set up a default TextPaint object
		mTextPaint = new TextPaint();
		mTextPaint.setFlags(Paint.ANTI_ALIAS_FLAG);
		mTextPaint.setTextAlign(Paint.Align.LEFT);

		// Update TextPaint and text measurements from attributes
		invalidateTextPaintAndMeasurements();
	}

	private void invalidateTextPaintAndMeasurements() {
		mTextPaint.setTextSize(getCounterFontSize());
		mTextPaint.setColor(getCounterFontColor());
		//mTextWidth = mTextPaint.measureText(Integer.toString(getCounter()));

		//Paint.FontMetrics fontMetrics = mTextPaint.getFontMetrics();
		//mTextHeight = fontMetrics.bottom;
	}

	@Override
	protected void onDraw(Canvas canvas) {

		super.onDraw(canvas);
		int paddingLeft = getPaddingLeft();
		int paddingTop = getPaddingTop();
		int paddingRight = getPaddingRight();
		int paddingBottom = getPaddingBottom();

		int contentWidth = getWidth() - paddingLeft - paddingRight;
		int contentHeight = getHeight() - paddingTop - paddingBottom;

		if (getImg() != null) {
			getImg().setBounds(paddingLeft, paddingTop,
					paddingLeft + contentWidth, paddingTop + contentHeight);
			getImg().draw(canvas);
		}
		Paint paint = new Paint();
		paint.setColor(getCounterBackgroundColor());


		float x, y, r;
		x = 0.85f*contentWidth;
		y = 0.85f*contentHeight;
		r = 0.15f*(contentWidth+contentHeight)/2f;

		canvas.drawCircle(x, y, r, paint);

//		 Draw the text.
		drawTextCentred(canvas, mTextPaint, Integer.toString(getCounter()), x, y);
	}



	private final Rect textBounds = new Rect();

	public void drawTextCentred(Canvas canvas, Paint paint, String text, float cx, float cy){
		paint.getTextBounds(text, 0, text.length(), textBounds);
		canvas.drawText(text, cx - textBounds.exactCenterX(), cy - textBounds.exactCenterY(), paint);
	}

	public int getCounterBackgroundColor() {
		return counterBackgroundColor;
	}

	public void setCounterBackgroundColor(int counterBackgroundColor) {
		this.counterBackgroundColor = counterBackgroundColor;
	}

	public float getCounterFontSize() {
		return counterFontSize;
	}

	public void setCounterFontSize(float counterFontSize) {
		this.counterFontSize = counterFontSize;
	}

	public int getCounter() {
		return counter;
	}

	public void setCounter(int counter) {
		this.counter = counter;
	}

	public Drawable getImg() {
		return img;
	}

	public void setImg(Drawable img) {
		this.img = img;
	}

	public int getCounterFontColor() {
		return counterFontColor;
	}

	public void setCounterFontColor(int counterFontColor) {
		this.counterFontColor = counterFontColor;
	}

	public boolean IsPressed() {
		return mIsPressed;
	}

	//TODO: Проработать для старых версий
	//@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	public void setPressed(boolean mIsPressed) {
		this.mIsPressed = mIsPressed;
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
			if (mIsPressed) {
				setBackground(getBackgroundPressed());
			} else {
				setBackground(getBackgroundNormal());
			}
		}

	}

	public Drawable getBackgroundPressed() {
		return backgroundPressed;
	}

	public void setBackgroundPressed(Drawable backgroundPressed) {
		this.backgroundPressed = backgroundPressed;
	}



	@Override
	public void onClick(View view) {
		Log.d(LOG_TAG, "onClick");
		if (!IsPressed() && counter <= 0)
			return;
		setPressed(!IsPressed());

		if (IsPressed()) {
			counter--;
		}
		else {
			counter++;
		}

		invalidate();
	}

	public Drawable getBackgroundNormal() {
		return backgroundNormal;
	}

	public void setBackgroundNormal(Drawable backgroundNormal) {
		this.backgroundNormal = backgroundNormal;
	}
}
