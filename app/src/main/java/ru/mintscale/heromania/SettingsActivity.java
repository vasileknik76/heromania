package ru.mintscale.heromania;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ToggleButton;

import java.util.Locale;

public class SettingsActivity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener {

    public static final String settingsName = "gamesettings";
    public static final String PARAM_LANG = "lang";
    public static final String PARAM_COINS = "coins";
    public static final String DEFAULT_LANG = "en";
    public static final String PARAM_BOOST_TIME = "boost_time";
    public static final String PARAM_BOOST_COIN = "boost_coin";
    public static final String PARAM_BOOST_LIFE = "boost_life";
    public static final int DEFAULT_COINS = 250;
    public static final int DEFAULT_BOOST_COUNT = 5;
    private static final String LANG_RU = "ru";
    private static final String LANG_EN = "en";
    private static final String LOG_TAG = "SettingsActivity";
    private static Context context;
    private ToggleButton lang_ru;
    private ToggleButton lang_en;
    private String language;
    private static boolean languageChanged = false;

    public static void init(Context ctx) {
        context = ctx;
    }

    public static void setLanguage(String language) {
        set(PARAM_LANG, language);
        setLanguageChanged(true);
    }

    public static String getLanguage() {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(PARAM_LANG, LANG_EN);
    }

    public static int getInt(String param) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt(param, 0);
    }

    public static int getCoins() {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt(PARAM_COINS, 0);
    }

    public static void setCoins(int value) {
        set(PARAM_COINS, value);
    }

    public static void set(String param, String value) {
        SharedPreferences.Editor e = PreferenceManager.getDefaultSharedPreferences(context).edit();
        e.putString(param, value);
        e.apply();
    }

    public static void set(String param, int value) {
        SharedPreferences.Editor e = PreferenceManager.getDefaultSharedPreferences(context).edit();
        e.putInt(param, value);
        e.commit();
        e.apply();
    }

    public static boolean isLanguageChanged() {
        return languageChanged;
    }

    public static void setLanguageChanged(boolean languageChanged) {
        SettingsActivity.languageChanged = languageChanged;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        init(this);
        language = getLanguage();
        lang_ru = (ToggleButton) findViewById(R.id.SA_toggle_RU);
        lang_en = (ToggleButton) findViewById(R.id.SA_toggle_EN);
        lang_ru.setChecked(language.equals(LANG_RU));
        lang_en.setChecked(language.equals(LANG_EN));
        lang_ru.setOnCheckedChangeListener(this);
        lang_en.setOnCheckedChangeListener(this);
        findViewById(R.id.SA_button_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        Log.d(LOG_TAG,String.format("isChecked: %b", isChecked));
        if (isChecked)
            if (buttonView.getId() == R.id.SA_toggle_RU) {
                language = LANG_RU;
                lang_en.setChecked(false);
            }
            else if (buttonView.getId() == R.id.SA_toggle_EN) {
                language = LANG_EN;
                lang_ru.setChecked(false);
            }
        else return;

        setLanguage(language);
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
        recreate();
    }
}
