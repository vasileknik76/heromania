package ru.mintscale.heromania;

/**
 * Created by nik on 23.03.2017.
 */

public interface OnAnswerListener {
	void onAnswer(int index);
}
