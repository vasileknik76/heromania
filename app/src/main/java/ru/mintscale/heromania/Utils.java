package ru.mintscale.heromania;

import android.content.Context;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by nik on 05.06.2016.
 */
public class Utils {

	//Читает файл из ассетов и возвращает его содержимое.
    public static String readFile(Context ctx, String fname) {
        StringBuilder buf=new StringBuilder();
        InputStream f = null;
        try {
            f = ctx.getAssets().open(fname);
        } catch (IOException e) {
            e.printStackTrace();
        }
        BufferedReader in=
                null;
        try {
            in = new BufferedReader(new InputStreamReader(f, "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String str;

        try {
            while ((str=in.readLine()) != null) {
                buf.append(str);
            }
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return buf.toString();
    }

	//Перемешивает символы в строке и возвращает новую строку
    public static String shuffle(String input){
        List<Character> characters = new ArrayList<Character>();
        for(char c:input.toCharArray()){
            characters.add(c);
        }
        StringBuilder output = new StringBuilder(input.length());
        while(characters.size()!=0){
            int randPicker = (int)(Math.random()*characters.size());
            output.append(characters.remove(randPicker));
        }
       return output.toString();
    }

    public static <T> ArrayList<T> shuffle(ArrayList<T> input, int length) {
        ArrayList<T> res = new ArrayList<>();
        int startlength = input.size();
        while (input.size()>startlength-length && input.size()>0) {
            int randPicker = (int)(Math.random()*input.size());
            res.add(input.remove(randPicker));
        }
        return res;
    }

    public static int getSubCount(Pattern p, String s) {
        Matcher matcher = p.matcher(s);
        int counter = 0;
        while(matcher.find()) {
            counter++;
        }
        return counter;
    }
}
