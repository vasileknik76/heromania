package ru.mintscale.heromania;

import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

public class QuizActivity extends AppCompatActivity implements OnAnswerListener{

	public static final String QUIZ_START_FRAGMENT_TAG = "QuizStartFragment";
	public static final String QUIZ_GAME_FRAGMENT_TAG = "QuizGameFragment";
	public static final String QUIZ_RESULT_FRAGMENT_TAG = "QuizResultFragment";


	//Максимальное количество вопросов.
	private static final int MAX_QUESTIONS = 30;

	//Период обновления таймера
	private static final long TIMER_PERIOD = 100;
	private static final String LOG_TAG = "QuizActivity";

	//Время игры по-умолчанию
	private static final long DEFAULT_GAME_DURATION = 60*1000;

	//Сколько добавляет времени буст "время"
	private static final long BOOST_TIME_DARATION = 10*1000;
	public static final float PRICE_PER_SECOND = 2;
	public static final int PRICE_PER_ANSWER = 1;
	public static final int PRICE_PER_LIFE = 5;

	//Длительность игры в миллисекундах
	private long gameDuration = DEFAULT_GAME_DURATION;

	//Количество правильных ответов
	private int answerRightCount = 0;

	//Количество неправильных ответов
	private int answerWrongCount = 0;

	//Индекс текущего вопроса
	private int currentQuestionIndex = 0;

	private ArrayList<Integer> questionsId;

	private FragmentManager fm;

	public static int money;
    public static Handler coinsUpdater;
    private TextView coins;

	private QuizStartFragment quizStartFragment;

	private DBMainHelper dbMainHelper;
	private QuizQuestion currentQuestion;
	private TextView titleTextView;
	public static Handler titleUpdater;

	private Timer timer;
	private QuizTimerTask timerTask;

	//Время хогда игра должна завершиться
	private long finishTime;

	//Количество жизней
	private int lifeCount = 3;

	//Календарь
	private Calendar calendar;
	private boolean isLifeBoost;
	private boolean isCoinBoost;

	//выиграл или проиграл.
	//Значение осуществляется по истечению времени или вопросов .
	private boolean isWin;
	private QuizResultFragment quizResultFragment;


	private View.OnClickListener resultFragmentListener;

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_start);

		//Создание Handler'ов для обновления информации
		titleTextView = (TextView) findViewById(R.id.text_quiz);
		titleUpdater = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				super.handleMessage(msg);
				if (msg.obj instanceof Long) {
					long n = (long) msg.obj;
					long seconds = n / 1000;
					long minutes = seconds / 60;
					seconds = seconds % 60;
					String text = String.format("%d:%02d",minutes, seconds);
					titleTextView.setText(text);
				} else {
					String s = (String) msg.obj;
					titleTextView.setText(s);
				}

			}
		};

		calendar = Calendar.getInstance();

		//Создаем фрагмент и мэнеджер фрагментов. Запускаем фрагмент
		fm = getSupportFragmentManager();
		showStartFragment();

		//Ставим обработчик на начало игры
		quizStartFragment.setStartListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				prepareNewGame();
				quizStartFragment.saveCounters();
				showQuestion();
			}
		});

		//Подключаемся к базе данных
		try {
			dbMainHelper = new DBMainHelper(this);
		} catch (PackageManager.NameNotFoundException e) {
			e.printStackTrace();
		}

		SettingsActivity.init(getApplicationContext());
		money = SettingsActivity.getInt(SettingsActivity.PARAM_COINS);
		coins = (TextView) findViewById(R.id.text_coins);
		coinsUpdater = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                coins.setText(String.valueOf(money));
            }
        };
		coinsUpdater.sendEmptyMessage(0);


		//Кнопка повторить или выход при завершении игры
		resultFragmentListener = new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (view.getId() == R.id.QRF_retry) {
					//показ фрагмента с выбором бустеров
					showStartFragment();
				} else if (view.getId() == R.id.QRF_quit) {
					finish();
				}
			}
		};
	}

    @Override
    protected void onPause() {
        super.onPause();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {

			if (quizStartFragment != null && !quizStartFragment.isVisible()) {
				showStartFragment();
				timer.cancel();
				return false;
			}

		}
		return super.onKeyDown(keyCode, event);
	}


	//Показывает фрагмент выбора бустов
	private void showStartFragment() {
		Message msg = new Message();
		msg.obj = getString(R.string.quiz);
		titleUpdater.sendMessage(msg);

		if (quizStartFragment == null) {
			quizStartFragment = QuizStartFragment.newInstance();
			fm.beginTransaction()
					//.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
					.add(R.id.QA_frame_root, quizStartFragment, QUIZ_START_FRAGMENT_TAG)
					.commit();
		}
		Fragment f = fm.findFragmentByTag(QUIZ_GAME_FRAGMENT_TAG);
		if (f != null && f.isVisible()) {

			quizStartFragment.resetBusters();
			quizStartFragment.readCounters();
			fm.beginTransaction()
					.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_right)
					.show(quizStartFragment)
					.remove(f)
					.commit();

		}
		if (quizResultFragment != null && quizResultFragment.isVisible()) {

			quizStartFragment.resetBusters();
			quizStartFragment.readCounters();
			fm.beginTransaction()
					.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_right)
					.show(quizStartFragment)
					.remove(quizResultFragment)
					.commit();

		}


	}


	@Override
	public void onBackPressed() {
		super.onBackPressed();

	}

	private void prepareNewGame() {
		questionsId = dbMainHelper.generateQuizQuestionsId(MAX_QUESTIONS);
		currentQuestionIndex = 0;
		answerWrongCount = 0;
		answerRightCount = 0;
		calendar = Calendar.getInstance();
		if (quizStartFragment.getLifeBoost().IsPressed()) {
			isLifeBoost = true;
			lifeCount = 4;
		} else {
			lifeCount = 3;
			isLifeBoost = false;
		}

		if (quizStartFragment.getCoinBoost().IsPressed()) {
			isCoinBoost = true;
		} else {
			isCoinBoost = false;
		}

		if (quizStartFragment.getTimeBoost().IsPressed()) {
			gameDuration = DEFAULT_GAME_DURATION+BOOST_TIME_DARATION;
		} else {
			gameDuration = DEFAULT_GAME_DURATION;
		}
		finishTime = calendar.getTimeInMillis()+gameDuration;
		timer = new Timer();
		timerTask = new QuizTimerTask();
		timer.schedule(timerTask, 0, TIMER_PERIOD);
	}

	private QuizQuestion getCurrentQuestionFromDB() {
		currentQuestion = dbMainHelper.getQuizQuestion(questionsId.get(currentQuestionIndex));
		return currentQuestion;
	}

	private void showQuestion() {
		QuizGameFragment f = QuizGameFragment.newInstance(getCurrentQuestionFromDB(), lifeCount);
		f.setOnAnserListener(this);
		f.setLifeBoost(isLifeBoost);
		if (quizStartFragment.isVisible()) {

			fm.beginTransaction()
					.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_left)
					.hide(quizStartFragment)
					//.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
					.add(R.id.QA_frame_root, f, QUIZ_GAME_FRAGMENT_TAG)
					.commit();
		} else {
			Fragment old = fm.findFragmentByTag(QUIZ_GAME_FRAGMENT_TAG);
			fm.beginTransaction()
					.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_left)
					.remove(old)
					//.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
					.add(R.id.QA_frame_root, f, QUIZ_GAME_FRAGMENT_TAG)
					.commit();
		}
	}


	//Пользователь дал ответ на вопрос
	@Override
	public void onAnswer(int index) {
		currentQuestionIndex++;
		if (index == currentQuestion.getCorrectIndex() && currentQuestionIndex < questionsId.size()) {
			answerRightCount++;
			showQuestion();
		} else if (index == currentQuestion.getCorrectIndex()) {
			answerRightCount++;
			isWin = true;
			showResultFragment();
		} else if (currentQuestionIndex < questionsId.size()){
			answerWrongCount++;
			lifeCount--;
			if (lifeCount == 0) {
				isWin = false;
				showResultFragment();
			}
			else {
				showQuestion();
			}
		} else {
			isWin = true;
			showResultFragment();
		}

	}


	//Показывает фрагмент с результатами.
	private void showResultFragment() {
		Log.d(LOG_TAG, "showResultFragment");
		timer.cancel();
		quizResultFragment = QuizResultFragment.newInstance();
		quizResultFragment.setResultClickListener(resultFragmentListener);
		quizResultFragment.setWin(isWin);
		quizResultFragment.setCoinBooster(isCoinBoost);
		quizResultFragment.setCorrectAnswersCount(answerRightCount);
		quizResultFragment.setLivesLeft(lifeCount);
		calendar = Calendar.getInstance();
		long elapsed =0;
		if (isWin)
			elapsed = (finishTime-calendar.getTimeInMillis());
		quizResultFragment.setTimeElapsed(elapsed);
		Fragment old = fm.findFragmentByTag(QUIZ_GAME_FRAGMENT_TAG);
		fm.beginTransaction()
				.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_left)
				.remove(old)
				//.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
				.add(R.id.QA_frame_root, quizResultFragment, QUIZ_RESULT_FRAGMENT_TAG)
				.commit();


	}

	//Следит за временем игры. Обновляет счетчик времени.
	private class QuizTimerTask extends TimerTask {
		@Override
		public void run() {
			calendar = Calendar.getInstance();
			long time = calendar.getTimeInMillis();
			Message msg = new Message();
			msg.obj = (finishTime - time);
			if (finishTime-time<=0) {
				isWin = false;
				showResultFragment();
				this.cancel();
			}
			titleUpdater.sendMessage(msg);
		}
	}
}
