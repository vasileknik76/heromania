package ru.mintscale.heromania;

import android.net.Uri;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link QuizGameFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link QuizGameFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class QuizGameFragment extends Fragment implements View.OnClickListener {

    private QuizQuestion question;
	private int lifeCount = 0;
	private boolean lifeBoost;


	//R.id кнопок с ответами
	private int[] answerButtons = new int[] {
			R.id.QGF_answer1,
			R.id.QGF_answer2,
			R.id.QGF_answer3,
			R.id.QGF_answer4
	};

	private int[] lifeImages = new int[] {
			R.id.QGF_heart1,
			R.id.QGF_heart2,
			R.id.QGF_heart3,
			R.id.QGF_heart4,
	};

	//Чтобы QuizActivity могла обрабатывать ответ пользователя
	private OnAnswerListener onAnswerListener;

	//Главный layout фрагмента
	private View rootView;

	public QuizGameFragment() {
        // Required empty public constructor
    }

    public static QuizGameFragment newInstance(QuizQuestion q, int lifeCount) {
        QuizGameFragment fragment = new QuizGameFragment();
        fragment.setQuestion(q);
	    fragment.setLifeCount(lifeCount);
        return fragment;
    }

	private void initButton(int index, String text) {
		Button b = (Button) rootView.findViewById(answerButtons[index]);
		b.setText(text);
		b.setOnClickListener(this);
	}

	private void initLives() {
		for (int i = 0; i < lifeImages.length; i++) {
			ImageView img = (ImageView) rootView.findViewById(lifeImages[i]);
			if (i < lifeCount)
				img.setImageDrawable(getResources().getDrawable(R.drawable.full_life));
			else
				img.setImageDrawable(getResources().getDrawable(R.drawable.no_life));

		}
	}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
	    rootView = inflater.inflate(R.layout.fragment_quiz_game, container, false);
	    ((TextView)rootView.findViewById(R.id.QGF_question)).setText(question.getQuestion());
	    int k = 0;
	    for (int i = 0; i < question.getCorrectIndex(); i++) {
			initButton(i, question.getWrongAnswer(k++));
	    }
	    initButton(question.getCorrectIndex(), question.getRightAnswer());
	    for (int i = question.getCorrectIndex()+1; i < answerButtons.length; i++) {
		    initButton(i, question.getWrongAnswer(k++));
	    }
	    initLives();
	    setLifeBoost(lifeBoost);
        return rootView;
    }

    public QuizQuestion getQuestion() {
        return question;
    }

    public void setQuestion(QuizQuestion question) {
        this.question = question;
    }

	public OnAnswerListener getOnAnserListener() {
		return onAnswerListener;
	}

	public void setOnAnserListener(OnAnswerListener onAnserListener) {
		this.onAnswerListener = onAnserListener;
	}

	@Override
	public void onClick(View view) {
		if (onAnswerListener == null)
			return;
		int id = view.getId();
		for (int i = 0; i < answerButtons.length; i++) {
			if (id == answerButtons[i])
				onAnswerListener.onAnswer(i);
		}
	}

	public void setLifeCount(int lifeCount) {
		this.lifeCount = lifeCount;
	}

	public boolean isLifeBoost() {
		return lifeBoost;
	}

	public void setLifeBoost(boolean lifeBoost) {
		this.lifeBoost = lifeBoost;
		if (rootView != null) {
			ImageView img = (ImageView) rootView.findViewById(lifeImages[lifeImages.length-1]);
			if (lifeBoost) {
				img.setVisibility(View.VISIBLE);
			} else {
				img.setVisibility(View.INVISIBLE);
			}
		}
	}


	public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
