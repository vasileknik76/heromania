package ru.mintscale.heromania;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link QuizStartFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link QuizStartFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class QuizStartFragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    private View.OnClickListener startListener;
	private BoostButton coinBoost;
	private BoostButton timeBoost;
	private BoostButton lifeBoost;


    public QuizStartFragment() {
        // Required empty public constructor
    }

    public static QuizStartFragment newInstance() {
        QuizStartFragment fragment = new QuizStartFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

	public void readCounters() {
		lifeBoost.setCounter(SettingsActivity.getInt(SettingsActivity.PARAM_BOOST_LIFE));
		timeBoost.setCounter(SettingsActivity.getInt(SettingsActivity.PARAM_BOOST_TIME));
		coinBoost.setCounter(SettingsActivity.getInt(SettingsActivity.PARAM_BOOST_COIN));
	}

	public void saveCounters() {
		SettingsActivity.set(SettingsActivity.PARAM_BOOST_LIFE, lifeBoost.getCounter());
		SettingsActivity.set(SettingsActivity.PARAM_BOOST_COIN, coinBoost.getCounter());
		SettingsActivity.set(SettingsActivity.PARAM_BOOST_TIME, timeBoost.getCounter());
	}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_quiz_start, container, false);
        root.findViewById(R.id.QSF_start).setOnClickListener(startListener);
		lifeBoost = (BoostButton) root.findViewById(R.id.QSA_life_boost);
		timeBoost = (BoostButton) root.findViewById(R.id.QSA_time_boost);
	    coinBoost = (BoostButton) root.findViewById(R.id.QSA_coin_boost);
	    readCounters();

        return root;
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public View.OnClickListener getStartListener() {
        return startListener;
    }

    public void setStartListener(View.OnClickListener startListener) {
        this.startListener = startListener;
    }

	public BoostButton getCoinBoost() {
		return coinBoost;
	}

	public void setCoinBoost(BoostButton coinBoost) {
		this.coinBoost = coinBoost;
	}

	public BoostButton getTimeBoost() {
		return timeBoost;
	}

	public void setTimeBoost(BoostButton timeBoost) {
		this.timeBoost = timeBoost;
	}

	public BoostButton getLifeBoost() {
		return lifeBoost;
	}

	public void setLifeBoost(BoostButton lifeBoost) {
		this.lifeBoost = lifeBoost;
	}

	public void resetBusters() {
		lifeBoost.setPressed(false);
		timeBoost.setPressed(false);
		coinBoost.setPressed(false);
	}

	/**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
