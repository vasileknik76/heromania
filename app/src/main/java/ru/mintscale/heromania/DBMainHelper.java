package ru.mintscale.heromania;

import android.content.ContentValues;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.preference.PreferenceManager;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

/**
 * Created by nik on 05.06.2016.
 */
//Класс для работы с базой SQLLite
public class DBMainHelper extends SQLiteOpenHelper {

    private static final String LOG_TAG = "DBMainHelper";
    private Context context;
    public DBMainHelper(Context context) throws PackageManager.NameNotFoundException {
        super(context, "main_"+PreferenceManager.getDefaultSharedPreferences(context).getString(SettingsActivity.PARAM_LANG, SettingsActivity.DEFAULT_LANG), null, context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d(LOG_TAG, "--- onCreate database ---");
        //Импорт кода базы из ассетов
        String code = Utils.readFile(context, "main_"+PreferenceManager.getDefaultSharedPreferences(context).getString(SettingsActivity.PARAM_LANG, SettingsActivity.DEFAULT_LANG)+".sql");
        Log.d(LOG_TAG, "--- "+"main_"+PreferenceManager.getDefaultSharedPreferences(context).getString(SettingsActivity.PARAM_LANG, SettingsActivity.DEFAULT_LANG)+".sql");
        Log.d(LOG_TAG, "code: "+code);

	    try {
		    db.setVersion(context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode);
	    } catch (PackageManager.NameNotFoundException e) {
		    e.printStackTrace();
	    }
	    //Поочередно выполняем запросы.
        String sqls[] = code.split(";");
        for (int i = 0; i<sqls.length; i++) {
            db.beginTransaction();
            Log.d(LOG_TAG, "sql: "+sqls[i]);
            db.execSQL(sqls[i]);
            db.setTransactionSuccessful();
            db.endTransaction();
        }

    }


	// TODO: обновление базы, в случае обновления приложения.
	// должно предусматривать сохранение прогресса при добавлении новых уровней, пакетов
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //Обновление базы
        Log.d(LOG_TAG, String.format("--- onUpgrade database --- from %d to %d", oldVersion, newVersion));
	    if (newVersion > oldVersion) {
		    db.execSQL("DELETE FROM level_guess");
		    db.execSQL("DELETE FROM part_to_level_guess");
		    db.execSQL("DELETE FROM quiz_question");
		    db.execSQL("DELETE FROM quiz_answer_right");
		    db.execSQL("DELETE FROM quiz_answer_wrong");
			onCreate(db);
	    }
    }


	//Загружаем список частей.
	// Для каждой части вызываем загрузку части. Т.е. Загружаем все уровни и сохраняем в parts
	public HashMap<Integer, ArrayList<LevelInfo>> loadGuessLevels() {
		HashMap<Integer, ArrayList<LevelInfo>> parts = new HashMap<>();
		SQLiteDatabase db = getReadableDatabase();
		Cursor c = db.rawQuery("SELECT distinct(partNumber) from part_to_level_guess", null);
		if (c.moveToFirst()) {
			do {
				int part = c.getInt(0);
				parts.put(part, loadGuessPart(part));
			} while (c.moveToNext());
		}
		return  parts;
	}

	public  ArrayList<LevelInfo> loadGuessPart(int partNumber) {
		ArrayList<LevelInfo> levels = new ArrayList<>();
		SQLiteDatabase db = getReadableDatabase();
		Cursor c = db.rawQuery("SELECT * from level_guess JOIN part_to_level_guess ON (level_guess.id=part_to_level_guess.levelId) WHERE partNumber="+partNumber, null);
		int colN = 0;
		int pos = 0;
		if (c.moveToFirst()) {
			int colPic = c.getColumnIndex("pic");
			int colAnswer = c.getColumnIndex("answer");
			int colPart = c.getColumnIndex("partNumber");
			int colLevelId = c.getColumnIndex("levelId");
			do {
				int id = c.getInt(colLevelId);
				int part = c.getInt(colPart);
				int level = pos;
				String answer = c.getString(colAnswer);
				String pic = c.getString(colPic);
				boolean completed;
				Cursor cur = db.rawQuery("SELECT * from level_guess_completed WHERE levelId="+id, null);
				completed = cur.moveToFirst();
				cur.close();
				LevelInfo li = new LevelInfo(part, level);
				li.setAnswer(answer);
				li.setPic(pic);
				li.setCompleted(completed);
				li.setId(id);
				cur = db.rawQuery("SELECT * from level_guess_save WHERE levelId="+id, null);
				if (cur.moveToFirst()) {
					String letters = cur.getString(cur.getColumnIndex("letters"));
					String fields = cur.getString(cur.getColumnIndex("fields"));
					String hintMask = cur.getString(cur.getColumnIndex("hintMask"));
					int u = cur.getInt(cur.getColumnIndex("isHalfWordUsed"));
					li.setHintHalfWordUsed((u == 1)?true:false);
					li.setHintMask(hintMask);
					li.setLettersSave(letters);
					li.setFieldsSave(fields);
				}
				cur.close();
				levels.add(li);
				pos++;
			} while (c.moveToNext());
		}
		c.close();
		return levels;
	}

	public void saveGuessLevels(ArrayList<LevelInfo> levels, int partNumber) {
		Log.d(LOG_TAG, "saveData()");
		SQLiteDatabase db = getWritableDatabase();
		db.execSQL(String.format("DELETE FROM level_guess_save WHERE levelId IN (SELECT levelId from part_to_level_guess WHERE partNumber=%d)", partNumber));
		db.execSQL(String.format("DELETE FROM level_guess_completed WHERE levelId IN (SELECT levelId from part_to_level_guess WHERE partNumber=%d)", partNumber));
		ContentValues cv = new ContentValues();
		for (int i = 0; i < levels.size(); i++) {
			LevelInfo l = levels.get(i);
			cv.clear();
			cv.put("levelId", l.getId());
			cv.put("letters", l.getLettersSave());
			cv.put("fields", l.getFieldsSave());
			cv.put("hintmask", l.getHintMask());
			int u = l.isHintHalfWordUsed() ? 1 : 0;
			cv.put("isHalfWordUsed", u);
			db.insert("level_guess_save", null, cv);

			if (l.isCompleted()) {
				cv.clear();
				cv.put("levelId", l.getId());
				db.insert("level_guess_completed", null, cv);
			}

		}
	}

	//Выбирает из базы вопросы и заносит n idшников в лист в случайном порядке
	public ArrayList<Integer> generateQuizQuestionsId(int n) {
		ArrayList<Integer> result = new ArrayList<>();
		SQLiteDatabase db = getReadableDatabase();
		Cursor c = db.rawQuery("SELECT count(*) as cnt FROM quiz_question", null);
		c.moveToFirst();
		//Получаем количество вопросов в базе
		int count = c.getInt(0);
		c.close();
		for (int i = 1; i <= count; i++)
			result.add(i);
		return Utils.shuffle(result, n);
	}


	public QuizQuestion getQuizQuestion(int id) {
		QuizQuestion q = new QuizQuestion();
		SQLiteDatabase db = getReadableDatabase();
		ArrayList<String> buffer = new ArrayList<String>();
		Cursor c = db.rawQuery(String.format("SELECT name FROM quiz_question WHERE id=%d", id), null);
		c.moveToFirst();
		if (c.getCount() > 0) {
			q.setQuestion(c.getString(0));
			Log.d(LOG_TAG, String.format("Вопрос №%d: %s", id, q.getQuestion()));
		}
		else {
			Log.d(LOG_TAG, String.format("Упс. Такого вопроса нет. Его номер №%d", id));
		}

		c.close();
		c = db.rawQuery(String.format("SELECT name FROM quiz_answer_right WHERE quiz_question_id=%d", id), null);
		c.moveToFirst();
		do {
			buffer.add(c.getString(0));
		} while (c.moveToNext());
		c.close();

		Random random = new Random();
		int i = (int)(Math.random()*buffer.size());
		q.setRightAnswer(buffer.get(i));


		c = db.rawQuery(String.format("SELECT name FROM quiz_answer_wrong WHERE quiz_question_id=%d", id), null);
		c.moveToFirst();
		buffer.clear();
		Log.d(LOG_TAG, String.format("Из базы пришло %d неправильных ответов", c.getCount()));
		do {
			buffer.add(c.getString(0));
		} while (c.moveToNext());
		c.close();
		//Получаем случайные 3 ответа;
		ArrayList<String> answers = Utils.shuffle(buffer, 3);
		i = (int)(Math.random()*(answers.size()+1));
		q.setCorrectIndex(i);
		Log.d(LOG_TAG, String.format("Отобрано %d неправильных ответов", answers.size()));
		q.setWrongAnswers(answers);
		Log.d(LOG_TAG, String.format("Индекс ответа: %d ", i));
		q.setId(id);
		return q;
	}
}

