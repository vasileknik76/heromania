package ru.mintscale.heromania;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by nik on 09.06.2016.
 */
public class LevelFragment extends Fragment implements View.OnClickListener{
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String LOG_TAG = "LevelFragment";
    private static final long COMPLETED_TIMEOUT = 1000;
    public static final int COLUMN_COUNT = 8;
    public static final int HINT_LETTER_COST = 10;
    public static final int HINT_REMOVE_WASTE_COST = 30;
    public static final int HINT_HALF_LETTER_COST = 50;
    public static final int HINT_LEVEL_COST = 10; //Сколько дают за уровень
    public static final String HINT_KEY = "hint";
    public static final int HINT_LETTER = 1; //Подсказка выбранная буква
    public static final int HINT_REMOVE_WASTE_LETTERS = 2; //Подсказка убрать лишние буквы
    public static final int HINT_HALF_WORD = 3; //подсказка отгадать половину букв слова
    public static final int HINT_DIALOG_OK = 1;
    public static final Character SYMBOL_NONE = '$';
    private static final int ANIMATION_WRONG_ANSWER_REPEAT_COUNT = 2;
    private static final int ANIMATION_WRONG_ANSWER_DURATION = 200;

    private int hint;
    private LevelInfo level;
    private LinearLayout lettersLayout; //Layout для letters и блока завершеннности уровня
    private String answer;
    private ArrayList<Button> fieldslist;
    private ArrayList<Button> letterslist;
    private int picHeight;
    private ImageButton btnHintLetter;
    private ImageButton btnHintWasteLetters;
    private ImageButton btnHintHalfWord;

    public LevelFragment() {

    }
    //boolean needed[]; //массив нужных букв

    public static final LevelFragment newInstance(LevelInfo level) {
        LevelFragment f = new LevelFragment();
        f.level = level;
        return f;
    }
    @Override
    public void onStop() {
        super.onStop();
        //Log.d(LOG_TAG, String.format("onStop   id: %d, position: %d", id, position));
    }

    @Override
    public void onPause() {
        super.onPause();
        //Log.d(LOG_TAG, String.format("onPause   id: %d, position: %d", id, position));
    }

    @Override
    public void onStart() {
        super.onStart();
        //Log.d(LOG_TAG, String.format("onStart   id: %d, position: %d", id, position));
    }

    @Override
    public void onResume() {
        super.onResume();
        //Log.d(LOG_TAG, String.format("onResume   id: %d, position: %d", id, position));
    }

    private static final String ARG_SECTION_NUMBER = "section_number";

    @Override
    public void onClick(View view) {
        if (level.isCompleted())
            return;
        Button b = (Button) view;
        //Нажали на поле
        if (fieldslist.contains(b)) {
            String text = (String) b.getText();
            if (text == null || text.isEmpty())
                return;
            Button l = null;
            //Ищем пустое место на поле букв
            String lS = level.getLettersSave();
            for (int i = 0; i < lS.length(); i++) {
                if (lS.charAt(i) == SYMBOL_NONE) {
                    l = letterslist.get(i);
                }
            }
            l.setText(b.getText());
            l.setBackgroundResource(R.drawable.letter_shape);
            b.setBackgroundResource(R.drawable.field_shape);
            b.setText("");
            SaveFields();
            SaveLetters();
        } else {  //Нажали на кнопку с буквой
            String text = (String) b.getText();
            if (text == null || text.isEmpty())
                return;
            int i;
            for (i = 0; i < fieldslist.size(); i++) {
                text = (String) fieldslist.get(i).getText();
                if (text == null || text.isEmpty())
                    break;
            }
            //Если все заполнено - выходим
            if (i>=fieldslist.size()) return;
            //Убираем букву на кнопке и ставим ее на поле
            Button f = fieldslist.get(i);
            f.setText(b.getText());
            f.setBackgroundResource(R.drawable.letter_shape);
            b.setBackgroundResource(R.drawable.field_shape);
            b.setText("");
            checkAnswer();
            SaveFields();
            SaveLetters();
        }

    }

    private void checkAnswer() {
        //Проверка слова
        String check = "";
        for (int j = 0; j < fieldslist.size(); j++) {
            check+= fieldslist.get(j).getText();
        }
        if (answer.length()== check.length()) {
            if (check.equals(answer)) {
                /** Деньги за уровень добавляются в {@link LevelFragment#prepareCompleted()} в Handler'e*/
                level.setCompleted(true);
                //nextLevel();
                prepareCompleted();
            } else {
                animateWrongAnswer();
            }
        }
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //position = getArguments().getInt(ARG_SECTION_NUMBER);
        //Log.d(LOG_TAG, String.format("onCreate   id: %d, position: %d", id, position));
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        //Вычисляем размер actionBar'a
        TypedValue tv = new TypedValue();

        int actionBarHeight = 0;
        if (getActivity().getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true))
        {
            actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data,getResources().getDisplayMetrics());
        }
        int margins = (int) (getResources().getDimension(R.dimen.level_image_margin)*2);
        picHeight = (size.y-actionBarHeight-margins)/2;


    }
    void fillImage(View rootView, String pic) {
        ImageView img = (ImageView) rootView.findViewById(R.id.image_level);
        img.getLayoutParams().height=picHeight;
        LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) img.getLayoutParams();

//        InputStream ims = null;
//        try {
//            ims =  getContext().getAssets().open(pic);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        Drawable d = Drawable.createFromStream(ims, null);
        Picasso.with(getContext()).load(String.format("file:///android_asset/%s", pic)).into(img);
//        img.setImageDrawable(d);
    }

    private void fillFields(View root, LayoutInflater inflater) {
        int rows = answer.length() / COLUMN_COUNT + ((answer.length()%COLUMN_COUNT > 0)?1:0);
        int rowlen = answer.length() / rows+((answer.length()%rows > 0)?1:0);
        int index = 0;
        int margin = (int) getResources().getDimension(R.dimen.margin_field);
        String fields = level.getFieldsSave();
        if (fields == null) {
            makeFields();
            fields = level.getFieldsSave();
        }
        String[] words = fields.split("[ \\-]+");

        for (int i = 0; i < words.length; i++) {
            LinearLayout ll = new LinearLayout(getContext());
            ll.setOrientation(LinearLayout.HORIZONTAL);
            LinearLayout.LayoutParams LLParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            LLParams.gravity = Gravity.CENTER;
            ll.setLayoutParams(LLParams);

            for (int j = 0; j < words[i].length(); j++, index++) {
                Button b = (Button) inflater.inflate(R.layout.field, ll, false);
                LinearLayout.LayoutParams lparams = (LinearLayout.LayoutParams) b.getLayoutParams();
                lparams.setMargins(margin, margin, margin, margin);
                b.setLayoutParams(lparams);

                fieldslist.add(b);
                b.setOnClickListener(this);

                if (words[i].charAt(j)!=SYMBOL_NONE) {
                    //Заполненная буква
                    b.setBackgroundResource(R.drawable.letter_shape);
                    b.setText(words[i].substring(j, j+1));
                }
                else {

                }
                //b.setText(answer.substring(index, index+1));
                //Если последняя кнопка ряда
                if (j + 1 == words[i].length()) {
                    b.setTag(13);
                }
                ll.addView(b);
            }
            ((ViewGroup) root).addView(ll);
        }

        String hintMask = level.getHintMask();
        if (hintMask == null) {
            makeHintMask();
            hintMask = level.getHintMask();
        }
        for (int i = 0; i < hintMask.length(); i++)
            if (hintMask.charAt(i)==SYMBOL_NONE)
                fieldslist.get(i).setOnClickListener(null);
    }

    private void fillLetters(View root, String answer, LayoutInflater inflater) {
        if (level.isCompleted())
            return;
        String letters = level.getLettersSave();
        if (letters == null) {
            makeLetters();
            letters = level.getLettersSave();
        }
        int rows = letters.length() / COLUMN_COUNT + ((letters.length()%COLUMN_COUNT > 0)?1:0);
        int rowlen = letters.length() / rows+((letters.length()%rows > 0)?1:0);

        int index = 0;
        int margin = (int) getResources().getDimension(R.dimen.margin_letter);
        for (int i = 0; i < rows; i++) {
            LinearLayout ll = new LinearLayout(getContext());
            ll.setOrientation(LinearLayout.HORIZONTAL);
            LinearLayout.LayoutParams LLParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            LLParams.gravity = Gravity.CENTER;
            ll.setLayoutParams(LLParams);
            Log.d(LOG_TAG, String.format("letters: %s, fields: %s, answer: %s",letters, level.getFieldsSave(), answer) );
            //размещение
            for (int j = 0; j < rowlen && index < letters.length(); j++, index++) {
                Button b = (Button) inflater.inflate(R.layout.letter, ll, false);
                LinearLayout.LayoutParams lparams = (LinearLayout.LayoutParams) b.getLayoutParams();
                lparams.setMargins(margin, margin, margin, margin);
                b.setLayoutParams(lparams);
                b.setOnClickListener(this);
                letterslist.add(b);
                if (letters.charAt(index) == SYMBOL_NONE) {
                    b.setBackgroundResource(R.drawable.field_shape);
                } else
                    b.setText(letters.substring(index, index+1));
                ll.addView(b);
            }
            ((ViewGroup) root).addView(ll);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle args) {
        Log.d(LOG_TAG, "onCreateView ");
        fieldslist = new ArrayList<Button>();
        letterslist = new ArrayList<Button>();
        View rootView = inflater.inflate(R.layout.fragment_level, container, false);
        rootView.setTag(level);
        String answer = level.getAnswer();
        this.answer = answer;
        Log.d(LOG_TAG, "name: "+answer);
        StringBuilder sb = new StringBuilder(this.answer);
        int i = 0;
        List<String> dict = Arrays.asList(getResources().getStringArray(R.array.letters));
        for(i = 0; i < sb.length(); i++) {
            if (!dict.contains(sb.substring(i, i+1))) {
                sb.deleteCharAt(i--);
            }
        }
        this.answer = sb.toString();

        //Заполняем изображение
        fillImage(rootView, level.getPic());
        //Добавление буков
        fillLetters(rootView.findViewById(R.id.letters_layout), level.getAnswer(), inflater);
        fillFields(rootView.findViewById(R.id.answer_layout), inflater);

        //Ставим обработчики на кнопки подсказок
        btnHintLetter = (ImageButton) rootView.findViewById(R.id.lf_hint_letter);
        btnHintLetter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setHint(HINT_LETTER);
            }
        });
        btnHintWasteLetters = (ImageButton) rootView.findViewById(R.id.lf_hint_waste_letters);
        btnHintWasteLetters.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setHint(HINT_REMOVE_WASTE_LETTERS);
            }
        });
        btnHintHalfWord = (ImageButton) rootView.findViewById(R.id.lf_hint_half_word);
        btnHintHalfWord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setHint(HINT_HALF_WORD);
            }
        });

        checkHintButtons();
        return rootView;
    }

    public void onShow() {
        checkHintButtons();
        Message msg = new Message();
        msg.obj = (level.getLevel()+1);
        GameActivity.titleUpdater.sendMessage(msg);
    }

    //Проверяет и блокирует/разблокирует кнопки подсказок
    private void checkHintButtons() {
        if (level.isCompleted() || level.isHintHalfWordUsed() || GameActivity.money<HINT_HALF_LETTER_COST)
            btnHintHalfWord.setImageDrawable(getResources().getDrawable(R.drawable.half_blocked));
        else
            btnHintHalfWord.setImageDrawable(getResources().getDrawable(R.drawable.half));
        if (level.isCompleted() || (letterslist.size() == fieldslist.size()) || GameActivity.money<HINT_REMOVE_WASTE_COST)
            btnHintWasteLetters.setImageDrawable(getResources().getDrawable(R.drawable.bomb_blocked));
        else
            btnHintWasteLetters.setImageDrawable(getResources().getDrawable(R.drawable.bomb));

        if (level.isCompleted() || GameActivity.money<HINT_LETTER_COST)
            btnHintLetter.setImageDrawable(getResources().getDrawable(R.drawable.letter_blocked));
        else
            btnHintLetter.setImageDrawable(getResources().getDrawable(R.drawable.letter));
    }

    private void makeFields() {
        List<String> dict = Arrays.asList(getResources().getStringArray(R.array.letters));
        StringBuilder s = new StringBuilder(level.getAnswer());
        for (int i = 0; i < s.length(); i++)
            if (dict.contains(s.substring(i, i+1)))
                s.setCharAt(i, SYMBOL_NONE);
        level.setFieldsSave(s.toString());
    }

    private  void makeHintMask() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < answer.length(); i++)
            sb.append(" ");
        level.setHintMask(sb.toString());
    }

//    //Функция генерации случайных букв
//    private void makeLetters() {
//        //отвечает за генерацию лишних буков:)
//        StringBuilder s = new StringBuilder(answer);
//        List<String> dict = Arrays.asList(getResources().getStringArray(R.array.letters));
////        int n = (478-13*answer.length())/31;
////        int all = answer.length()+n;
////        Log.d(LOG_TAG, String.format("answer: %d, add: %d, all: %d", all-n, n, all));
////
////        int rows = 2;
////        for (rows = 1; ;rows++) {
////            if (all<rows*COLUMN_COUNT)
////                break;
////        }
////
////        if (all % rows != 0) {
////            n += rows - all % rows;
////        }
//        int n = (COLUMN_COUNT-answer.length() % COLUMN_COUNT)%COLUMN_COUNT;
//        for (int i = 0; i < n; i++) {
//            int rand = (int) (Math.random()*dict.size());
//            s.append(dict.get(rand));
//        }
//        level.setLettersSave(Utils.shuffle(s.toString()));
//    }

    //Функция генерации случайных букв
    private void makeLetters() {
        //отвечает за генерацию лишних буков:)
        StringBuilder s = new StringBuilder(answer);
        List<String> dict = Arrays.asList(getResources().getStringArray(R.array.letters));
        int n;
        if (answer.length()<COLUMN_COUNT) {
            n = 2*COLUMN_COUNT-answer.length();
        } else if (answer.length()<2*COLUMN_COUNT) {
            n = 2*COLUMN_COUNT-answer.length();
            if (n<2)
                n+= 2;
        } else {
            n = 3*COLUMN_COUNT-answer.length();
            if (n<1)
                n+= 2;
        }
        for (int i = 0; i < n; i++) {
            int rand = (int) (Math.random()*dict.size());
            s.append(dict.get(rand));
        }
        level.setLettersSave(Utils.shuffle(s.toString()));
    }

    public void setHint(int hint) {
        this.hint = hint;
        if (level.isCompleted())
            return;
        int cost = 0;
        //Обработка подсказки
        if (hint == HINT_LETTER) {
            clearFields();

            //Есть ли деньги
            if (GameActivity.money<HINT_LETTER_COST)
                return;
            cost = HINT_LETTER_COST;

            //кол-во букв
            StringBuilder fields = new StringBuilder(level.getFieldsSave().replaceAll(" ","").replaceAll("-", ""));
            StringBuilder letters = new StringBuilder(level.getLettersSave());
            StringBuilder hintMask = new StringBuilder(level.getHintMask());
            if (fields.indexOf("$") == -1)
                return;
            int n = 1;
            for (int i = 0; i < n; i++) {
                //Индекс буквы на поле.
                int f_index = (int) (Math.random()*fieldslist.size());
                //Если клетка поля не пуста то начинаем сначала
                if (fields.charAt(f_index) != SYMBOL_NONE) {
                    i--; continue;
                }
                //Клетка пуста. Ищем кнопку с нужной буквой (answer[i])
                for (int j = 0; j < letters.length(); j++) {
                    if (letters.charAt(j) == answer.charAt(f_index)) {
                        //Делаем замену
                        letterslist.get(j).setText("");
                        fieldslist.get(f_index).setText(String.valueOf(letters.charAt(j)));
                        letterslist.get(j).setBackgroundResource(R.drawable.field_shape);
                        fieldslist.get(f_index).setBackgroundResource(R.drawable.letter_shape);
                        fieldslist.get(f_index).setOnClickListener(null);
                        letters.setCharAt(j, SYMBOL_NONE);
                        fields.setCharAt(f_index, answer.charAt(f_index));
                        hintMask.setCharAt(f_index, SYMBOL_NONE);
                        break;
                    }
                }
            }
            checkAnswer();
            level.setHintMask(hintMask.toString());
            level.setLettersSave(letters.toString());
            level.setFieldsSave(fields.toString());
            SaveFields();

        } else if (hint == HINT_REMOVE_WASTE_LETTERS) {
            if (letterslist.size() == fieldslist.size())
                return;
//            clearFields();
            //Есть ли деньги
            if (GameActivity.money<HINT_REMOVE_WASTE_COST)
                return;
            cost = HINT_REMOVE_WASTE_COST;
            ArrayList<Button> superlist = new ArrayList<>();
            for (int i = 0; i < letterslist.size(); i++)
                superlist.add(letterslist.get(i));
            for (int i = 0; i < fieldslist.size(); i++)
                superlist.add(fieldslist.get(i));
            int lLen = letterslist.size()-1;

            String answer = new String(this.answer);
            String allLetters = new String(level.getLettersSave());
            allLetters+= level.getFieldsSave();
            allLetters = allLetters.replaceAll("[ $\\-]*", "");
            for (int i = 0; i < answer.length(); i++) {
                allLetters = allLetters.replaceFirst(answer.substring(i, i+1), "");
            }
            //Количество кнопок в letters которые надо удалить
            int letters_to_remove = 0;
            for (int j = 0; j < allLetters.length(); j++) {
                for (int i = 0; i < superlist.size(); i++) {
                    Button b = superlist.get(i);
                    String text = (String) b.getText();
                    if (text.isEmpty())
                        continue;
                    if (text.charAt(0)==allLetters.charAt(j)) {
                        //Удаляем кнопку из Letters. А если в fields то чистим
                        //Если в fields
                        if (i>lLen) {
                            //int f_index = fieldslist.indexOf(b);
                            b.setBackgroundResource(R.drawable.field_shape);
                            b.setText("");
                            letters_to_remove++;
                        } else { //Если в letters
                            letterslist.remove(b);
                            ((ViewGroup)b.getParent()).removeView(b);
                        }
                        superlist.remove(i);
                        i--; lLen--;
                        break;
                    }
                }
            }
            int k = 0;
            for (int i = 0; i < letters_to_remove; i++) {
                for (int j = k; j < letterslist.size(); j++) {
                    Button b = letterslist.get(j);
                    String text = (String) b.getText();
                    if (text.equals("")) {
                        //Нашли пустую кнопку - удаляем ее
                        letterslist.remove(b);
                        ((ViewGroup)b.getParent()).removeView(b);
                        k = j;
                        break;
                    }
                }
            }
            SaveLetters();
            SaveFields();
        } else if (hint == HINT_HALF_WORD) {
            if (level.isHintHalfWordUsed())
                return;
            //Есть ли деньги
            if (GameActivity.money<HINT_HALF_LETTER_COST)
                return;
            cost = HINT_HALF_LETTER_COST;
            clearFields();
            //кол-во букв
            StringBuilder fields = new StringBuilder(level.getFieldsSave().replaceAll(" ","").replaceAll("-", ""));
            StringBuilder letters = new StringBuilder(level.getLettersSave());
            StringBuilder hintMask = new StringBuilder(level.getHintMask());
            int available_fields = Utils.getSubCount(Pattern.compile("\\$"), fields.toString());
            int n = (int) Math.ceil(fieldslist.size()/2.0);
            if (available_fields<n)
                n = available_fields;

            for (int i = 0; i < n; i++) {
                //Индекс буквы на поле.
                int f_index = (int) (Math.random()*fieldslist.size());
                //Если клетка поля не пуста то начинаем сначала
                if (fields.charAt(f_index) != SYMBOL_NONE) {
                    i--; continue;
                }
                //Клетка пуста. Ищем кнопку с нужной буквой (answer[i])
                for (int j = 0; j < letters.length(); j++) {
                    if (letters.charAt(j) == answer.charAt(f_index)) {
                        //Делаем замену
                        letterslist.get(j).setText("");
                        fieldslist.get(f_index).setText(String.valueOf(letters.charAt(j)));
                        letterslist.get(j).setBackgroundResource(R.drawable.field_shape);
                        fieldslist.get(f_index).setBackgroundResource(R.drawable.letter_shape);
                        fieldslist.get(f_index).setOnClickListener(null);
                        letters.setCharAt(j, SYMBOL_NONE);
                        fields.setCharAt(f_index, answer.charAt(f_index));
                        hintMask.setCharAt(f_index, SYMBOL_NONE);
                        break;
                    }
                }
            }
            checkAnswer();
            level.setHintHalfWordUsed(true);
            level.setHintMask(hintMask.toString());
            level.setLettersSave(letters.toString());
            level.setFieldsSave(fields.toString());
            SaveFields();
        }
        GameActivity.money -= cost;
        Message msg = new Message();
        GameActivity.coinsUpdater.sendMessage(msg);
        checkHintButtons();
    }

    //Переносит все буквы на поле обратно на свои места.
    private void clearFields() {
        StringBuilder letters = new StringBuilder(level.getLettersSave());
        StringBuilder fields = new StringBuilder(level.getFieldsSave().replace(" ", "").replace("-",""));
        String hintMask = level.getHintMask();
        int k = 0;
        for (int i = 0; i < fields.length(); i++) {
            if (fields.charAt(i) != SYMBOL_NONE && hintMask.charAt(i) != SYMBOL_NONE) {
                for (int j = k; j < letters.length(); j++) {
                    if (letters.charAt(j) == SYMBOL_NONE) {
                        k = j;
                        letters.setCharAt(j, fields.charAt(i));
                        fields.setCharAt(i, SYMBOL_NONE);
                        letterslist.get(j).setText(String.valueOf(letters.charAt(j)));
                        fieldslist.get(i).setText("");
                        letterslist.get(j).setBackgroundResource(R.drawable.letter_shape);
                        fieldslist.get(i).setBackgroundResource(R.drawable.field_shape);
                        break;
                    }
                }
            }
        }
        level.setLettersSave(letters.toString());
        level.setFieldsSave(fields.toString());
    }

    private void SaveLetters() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < letterslist.size(); i++) {
            String text = (String) letterslist.get(i).getText();
            if (text.isEmpty())
                sb.append(SYMBOL_NONE);
            else
                sb.append(text);
        }
        level.setLettersSave(sb.toString());
    }

    private void SaveFields() {
        StringBuilder sb = new StringBuilder();
        int top = fieldslist.get(0).getTop();
        for (int i = 0; i < fieldslist.size(); i++) {
            String text = (String) fieldslist.get(i).getText();
            if (text.isEmpty())
                sb.append(SYMBOL_NONE);
            else
                sb.append(text);
            Object tag = fieldslist.get(i).getTag();
            if (tag != null && ((int) tag==13) && (i + 1 != fieldslist.size())) {
                sb.append(" ");
            }
        }
        level.setFieldsSave(sb.toString());
    }

    public int getHint() {
        return hint;
    }

    private void animateWrongAnswer() {
        ValueAnimator animator = ValueAnimator.ofFloat(0, 1);
        animator.setRepeatCount(ANIMATION_WRONG_ANSWER_REPEAT_COUNT);
        animator.setRepeatMode(ValueAnimator.RESTART);
        animator.setDuration(ANIMATION_WRONG_ANSWER_DURATION);
        animator.setInterpolator(new LinearInterpolator());
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            private int oldPos = -1;
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float f_pos = (Float) animation.getAnimatedValue();
                int pos;
                if (f_pos > .5)
                    pos = 1;
                else
                    pos = 0;
                if (pos != oldPos) {
                    int color;
                    if (pos == 0)
                        color = getResources().getColor(R.color.colorRed);
                    else
                        color = getResources().getColor(R.color.colorWhite);
                    for (Button b: fieldslist) {
                        b.setTextColor(color);
                    }
                    oldPos = pos;
                }
            }
        });
        animator.start();
    }

    private void animatePagerTransition(int dx) {
        final boolean forward = (dx > 0) ? true : false;
        dx = forward ? dx : -dx;
        ValueAnimator animator = ValueAnimator.ofInt(0, dx*GameActivity.mViewPager.getWidth() - ( forward ? GameActivity.mViewPager.getPaddingLeft() : GameActivity.mViewPager.getPaddingRight() )-1);
        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                GameActivity.mViewPager.endFakeDrag();
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                GameActivity.mViewPager.endFakeDrag();
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }
        });


        animator.setInterpolator(new AccelerateInterpolator());
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            private int oldDragPosition = 0;

            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                int dragPosition = (Integer) animation.getAnimatedValue();
                int dragOffset = dragPosition - oldDragPosition;
                oldDragPosition = dragPosition;
//                GameActivity.mViewPager.fakeDragBy(100);
                GameActivity.mViewPager.fakeDragBy(dragOffset * (forward ? -1 : 1));
            }
        });

        animator.setDuration(GameActivity.SCROLL_TIMEOUT*dx);
        GameActivity.mViewPager.beginFakeDrag();
        animator.start();
    }

    //Переходит к следующему уровню или возвращает в меню
    public void nextLevel() {
        int part = level.getPart();

        int levelNum = level.getLevel()+1;
        for (;;) {
            if (levelNum == GameActivity.parts.get(part).size()) {
                GameActivity.dbMainHelper.saveGuessLevels(GameActivity.parts.get(part), part);
                //fm.beginTransaction().add(R.id.frame_root, blockFragment, "block").commit();
                GameActivity.fm.beginTransaction().show(GameActivity.blockFragment).commitAllowingStateLoss();
                Message msg = new Message();
                msg.obj = getResources().getString(R.string.level_choice);
                GameActivity.titleUpdater.sendMessage(msg);
                break;
            } else {
                LevelInfo l = GameActivity.parts.get(part).get(levelNum);
                if (l.isCompleted()) {
                    levelNum++;
                    continue;
                } else {
                    //GameActivity.mViewPager.setCurrentItem(l.getLevel(), true);
                    animatePagerTransition(l.getLevel()-level.getLevel());
                    break;
                }
            }
        }

//        if (level.getLevel()+1 == GameActivity.parts.get(part).size()) {
//            GameActivity.saveData(GameActivity.parts.get(part), part);
//            //fm.beginTransaction().add(R.id.frame_root, blockFragment, "block").commit();
//            GameActivity.fm.beginTransaction()
//                    .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_in_left)
//                    .show(GameActivity.blockFragment).commit();
//            Message msg = new Message();
//            msg.obj = getResources().getString(R.string.level_choice);
//            GameActivity.titleUpdater.sendMessage(msg);
//        } else {
////            GameActivity.mViewPager.setCurrentItem(level.getLevel() + 1, true);
//            Log.d(LOG_TAG, "before  animatePagerTransition");
//            animatePagerTransition(1);
//            Log.d(LOG_TAG, "after  animatePagerTransition");
//        }
    }

    //Удаляет letters. Вместо этого добавляет другой layout.
    public void prepareCompleted() {
        Button b = letterslist.get(0);
        LinearLayout l_hor = (LinearLayout) b.getParent();
        LinearLayout root = (LinearLayout) l_hor.getParent();
        root.removeAllViews();
        lettersLayout = root;
        letterslist.clear();
        //Создаем Новый кусок.
        LinearLayout ll = (LinearLayout) getLayoutInflater(null).inflate(R.layout.level_completed_layout, root, false);
        root.addView(ll);
        Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.level_completed);
        ll.startAnimation(animation);
        GameActivity.setIsPageTracking(true);
        Handler scroll_handler = new Handler();
        scroll_handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Даем деньги за уровень
                GameActivity.money+= HINT_LEVEL_COST;
                Message msg = new Message();
                GameActivity.coinsUpdater.sendMessage(msg);
                if (!GameActivity.isPageChanged())
                    nextLevel();
                GameActivity.setIsPageTracking(false);
            }
        }, COMPLETED_TIMEOUT);
        //Ощищает информацию о пройденном уровне после скрола
        scroll_handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                clearCompleted();
            }
        }, COMPLETED_TIMEOUT+GameActivity.SCROLL_TIMEOUT);
    }

    //Удаляет информацию о прохождении уровня
    private void clearCompleted() {
        lettersLayout.removeAllViews();
    }
}
