package ru.mintscale.heromania;

/**
 * Created by nik on 07.06.2016.
 */
public class LevelInfo {
    private int part;
    private int level; //Индекс отнносительно части
    private String answer;
    private String pic;
    private boolean completed;
    private boolean available;
    private int id;
    private String lettersSave;
    private String fieldsSave;
    private boolean isHintHalfWordUsed;
    private String hintMask; //Без пробелов. Просто поля. символ $ если подсказка использовалась

    public int getPart() {
        return part;
    }

    public void setPart(int part) {
        this.part = part;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public LevelInfo(int part, int level) {
        this.part = part;
        this.level = level;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public String getLettersSave() {
        return lettersSave;
    }

    public void setLettersSave(String lettersSave) {
        this.lettersSave = lettersSave;
    }

    public String getFieldsSave() {
        return fieldsSave;
    }

    public void setFieldsSave(String fieldsSave) {
        this.fieldsSave = fieldsSave;
    }

    public boolean isHintHalfWordUsed() {
        return isHintHalfWordUsed;
    }

    public void setHintHalfWordUsed(boolean hintHalfWordUsed) {
        isHintHalfWordUsed = hintHalfWordUsed;
    }

    public String getHintMask() {
        return hintMask;
    }

    public void setHintMask(String hintMask) {
        this.hintMask = hintMask;
    }
}
