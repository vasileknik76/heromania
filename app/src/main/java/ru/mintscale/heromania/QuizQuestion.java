package ru.mintscale.heromania;

import java.util.ArrayList;

/**
 * Created by nik on 23.03.2017.
 */

public class QuizQuestion {
	private int id;
	private String question;
	private String rightAnswer;
	private ArrayList<String> wrongAnswers;

	//Индекс кнопки с корректным ответом.
	private int correctIndex;
	public QuizQuestion() {

	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getRightAnswer() {
		return rightAnswer;
	}

	public void setRightAnswer(String rightAnswer) {
		this.rightAnswer = rightAnswer;
	}

	public ArrayList<String> getWrongAnswers() {
		return wrongAnswers;
	}

	public String getWrongAnswer(int i) {
		return wrongAnswers.get(i);
	}

	public void setWrongAnswers(ArrayList<String> wrongAnswers) {
		this.wrongAnswers = wrongAnswers;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCorrectIndex() {
		return correctIndex;
	}

	public void setCorrectIndex(int correctIndex) {
		this.correctIndex = correctIndex;
	}
}
