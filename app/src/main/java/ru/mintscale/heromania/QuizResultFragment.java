package ru.mintscale.heromania;

import android.net.Uri;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link QuizResultFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link QuizResultFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class QuizResultFragment extends Fragment {


    private boolean coinBooster = false;
	private boolean win = false;
	private long timeElapsed;
	private int correctAnswersCount;
	private int livesLeft;
	private View rootView;

	private View.OnClickListener resultClickListener;

	public QuizResultFragment() {
        // Required empty public constructor
    }

    public static QuizResultFragment newInstance() {
        QuizResultFragment fragment = new QuizResultFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_quiz_result, container, false);
	    TextView tv = (TextView) rootView.findViewById(R.id.QRF_text);
	    String s = "";
	    int earn = 0;
	    Message msg = new Message();
	    if (win)
			msg.obj = getString(R.string.win);
	    else
	        msg.obj = getString(R.string.lose);

	    QuizActivity.titleUpdater.sendMessage(msg);

	    s+= String.format("%s: %d\n", getString(R.string.result_correct), correctAnswersCount);
	    earn+= correctAnswersCount * QuizActivity.PRICE_PER_ANSWER;
	    if (timeElapsed>0) {
		    int tb = (int) (timeElapsed/1000*QuizActivity.PRICE_PER_SECOND);
		    s+= String.format("%s: %d\n", getString(R.string.result_time_bonus), tb);
			earn += tb;
	    }

	    if (win) {
		    int lb = livesLeft*QuizActivity.PRICE_PER_LIFE;
		    s+= String.format("%s: %d\n", getString(R.string.result_life_bonus), lb);
		    earn += lb;
	    }

	    if (!win) {
		    int lp = earn / -2;
		    s+= String.format("%s: %d\n", getString(R.string.result_penalty), lp);
		    earn += lp;
	    }

	    if (isCoinBooster()) {
		    int cb = earn;
		    s+= String.format("%s: x2\n", getString(R.string.result_boost));
		    earn += cb;
	    }

	    s+= String.format("%s: %d", getString(R.string.result_total), earn);
	    tv.setText(s);
	    QuizActivity.money+=earn;
	    SettingsActivity.set(SettingsActivity.PARAM_COINS, QuizActivity.money);
	    QuizActivity.coinsUpdater.sendEmptyMessage(0);
		rootView.findViewById(R.id.QRF_quit).setOnClickListener(resultClickListener);
	    rootView.findViewById(R.id.QRF_retry).setOnClickListener(resultClickListener);
        return rootView;
    }


    @Override
    public void onDetach() {
        super.onDetach();
    }

	public boolean isCoinBooster() {
		return coinBooster;
	}

	public void setCoinBooster(boolean coinBooster) {
		this.coinBooster = coinBooster;
	}

	public boolean isWin() {
		return win;
	}

	public void setWin(boolean win) {
		this.win = win;
	}

	public long getTimeElapsed() {
		return timeElapsed;
	}

	public void setTimeElapsed(long timeElapsed) {
		this.timeElapsed = timeElapsed;
	}

	public int getCorrectAnswersCount() {
		return correctAnswersCount;
	}

	public void setCorrectAnswersCount(int correctAnswersCount) {
		this.correctAnswersCount = correctAnswersCount;
	}

	public int getLivesLeft() {
		return livesLeft;
	}

	public void setLivesLeft(int livesLeft) {
		this.livesLeft = livesLeft;
	}

	public View.OnClickListener getResultClickListener() {
		return resultClickListener;
	}

	public void setResultClickListener(View.OnClickListener resultClickListener) {
		this.resultClickListener = resultClickListener;
	}


	public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
