package ru.mintscale.heromania;

import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;


public class BlockFragment extends Fragment {

    private static final String LOG_TAG = "BlockFragment";
    public static final int COLUMN_NUMBER = 3;

    private ViewGroup root;
    private LayoutInflater inflater;
    public static int blockSize;
    private ListView lv;


    public BlockFragment() {
        // Required empty public constructor
    }

    //singleton
    public static BlockFragment newInstance() {
        BlockFragment fragment = new BlockFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(LOG_TAG, "onCreate");
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        blockSize = (int) ((size.x - 2*(COLUMN_NUMBER+1)*getResources().getDimension(R.dimen.margin_part))/COLUMN_NUMBER);

    }

    void updateViews() {
        int partInfo = -1;
        int colN = 0;
        boolean available = false;
        double c = 0;
        int a = 0;

        for (int part = 1; part<=GameActivity.parts.size();part++) {

            if (part == 1)
                available = true;
            else
                available = (c / a >= GameActivity.PERCENT_COMPLETED);
            int need = (int) Math.ceil(GameActivity.PERCENT_COMPLETED * a - c);

            ArrayList<LevelInfo> levels = GameActivity.parts.get(part);
            for (int i = 0; i < levels.size(); i++) {
                if (levels.get(i).isCompleted())
                    c++;
                a++;
            }
            if (!available) {
                String tag = "part_tv:"+part;
                TextView tv = (TextView) root.findViewWithTag(tag);
                tv.setText(String.format("%s %d (Осталось %d)", getResources().getString(R.string.part_label), part, need));

            }
            boolean part_tv_change = false;

            for (LevelInfo l : levels) {
                ImageView img = (ImageView) root.findViewWithTag(l);
                FrameLayout img_layout = (FrameLayout) img.getParent();

                if (!l.isAvailable() && available) {
                    if (!part_tv_change) {
                        String tag = "part_tv:"+part;
                        TextView tv = (TextView) root.findViewWithTag(tag);
                        tv.setText(getResources().getString(R.string.part_label)+" "+part);
                    }
                    //Уровень стал доступен
                    l.setAvailable(available);
                    img.setOnClickListener(GameActivity.onLevelSelected);
                    img_layout.setForeground(getResources().getDrawable(R.drawable.border_image));
                }

                if (l.isCompleted())
                    img_layout.setForeground(getResources().getDrawable(R.drawable.completed_image));
            }

        }
        root.invalidate();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        Log.d(LOG_TAG, "onHiddenChanged: "+hidden);
        if (!hidden) {
////            updateViews();
            ((BlockFragmentAdapter) lv.getAdapter()).notifyDataSetChanged();
            lv.invalidate();
            GameActivity.setIsPageChanged(true);
        }
    }

    private void createViews() {
        int partInfo = -1;
        LinearLayout ll = null;
        int colN = 0;
        boolean available = false;
        double c = 0;
        int a = 0;

        for (int part = 1; part<=GameActivity.parts.size();part++) {

            if (part == 1)
                available = true;
            else
                available = (c/a>=GameActivity.PERCENT_COMPLETED);
            int need = (int) Math.ceil(GameActivity.PERCENT_COMPLETED * a - c);

            ArrayList<LevelInfo> levels = GameActivity.parts.get(part);
            for (int i = 0; i < levels.size(); i++) {
                if (levels.get(i).isCompleted())
                    c++;
                a++;
            }

            //Выводим номер части
            TextView tv = new TextView(getContext());
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            int m = (int) getResources().getDimension(R.dimen.margin_part);
            lp.setMargins(m, m, m, m);
            tv.setLayoutParams(lp);
            String tag = "part_tv:"+part;
            tv.setTag(tag);
            tv.setTextSize(getResources().getDimension(R.dimen.ba_tv_part_textsize));
            if (available)
                tv.setText(getResources().getString(R.string.part_label)+" "+part);
            else
                tv.setText(String.format("%s %d (Осталось %d)", getResources().getString(R.string.part_label), part, need));
            root.addView(tv);
            colN = 0;

            for (LevelInfo l : levels) {
                l.setAvailable(available);
                if (colN == 0) {
                    ll = new LinearLayout(getContext());
                    ll.setOrientation(LinearLayout.HORIZONTAL);
                    LinearLayout.LayoutParams LLParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    //ll.setGravity(Gravity.CENTER);
                    m = (int) getResources().getDimension(R.dimen.margin_part);
                    LLParams.setMargins(m,0,m,0);
                    LLParams.gravity = Gravity.CENTER_HORIZONTAL;
                    ll.setLayoutParams(LLParams);
                    root.addView(ll);
                }
                ImageView img;
                FrameLayout img_layout;

                img_layout = (FrameLayout) inflater.inflate(R.layout.ba_level_image, ll, false);
                img = (ImageView) img_layout.findViewById(R.id.part_img_level);
                if (!l.isAvailable())
                    img_layout.setForeground(getResources().getDrawable(R.drawable.locked_image));
                else if (l.isCompleted()) {
                    //Рисуем галочку
                    img_layout.setForeground(getResources().getDrawable(R.drawable.completed_image));
                } else
                    img_layout.setForeground(getResources().getDrawable(R.drawable.border_image));

                img.setTag(l);
                if (l.isAvailable())
                    img.setOnClickListener(GameActivity.onLevelSelected);
                InputStream ims = null;
                try {
                    ims =  getContext().getAssets().open(l.getPic());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                BitmapDrawable d = (BitmapDrawable) Drawable.createFromStream(ims, null);
                img.setImageDrawable(d);

                ll.addView(img_layout);
                colN = (colN + 1) % COLUMN_NUMBER;
            }

        }
    }

    //Создание элементов фрагмента
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(LOG_TAG, "onCreateViews");
        //Создаем список уровней
        View item = (ViewGroup) inflater.inflate(R.layout.fragment_block_with_list, container, false);
        lv = (ListView) item.findViewById(R.id.listView1);
        //Создаем новый адаптер для списка, он заполняет список картинками
        // и ставит обработчики
        lv.setAdapter(new BlockFragmentAdapter(getContext(), GameActivity.parts));
        lv.setScrollingCacheEnabled(false);
        return item;
    }
}
