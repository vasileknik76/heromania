package ru.mintscale.heromania;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;

import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by vasileknik76 on 16.06.2016.
 */
public class BlockFragmentAdapter extends BaseAdapter{

	//Маска отображающая является ли элемент списка TextView
	private static final int TV_MASK = 0x800000;
    @Override
    public void notifyDataSetChanged() {
        checkChanges();
        super.notifyDataSetChanged();
    }
	// Маска для вычисления части по позиции
    private static final int PART_MASK = 0xFF;
	// Маска для вычисления уровня по позиции
    private static final int LEVEL_MASK = 0xFF00;

    private LayoutInflater lInflater;
    private Context context;

    /**
     * Если выставлен бит {@link BlockFragmentAdapter#TV_MASK} значит это textView для части. Номер части можно получить по маске
     * {@link BlockFragmentAdapter#PART_MASK}
     * Количество уровней для открытия можно получить из карты needed
     * Если не выставлен - то это строка. Номер части можно получить по маске {@link BlockFragmentAdapter#PART_MASK},
     * индекс первого элемента по маске {@link BlockFragmentAdapter#LEVEL_MASK}
     *
     */
    ArrayList<Integer> rows;
    HashMap<Integer, Integer> needed;
    HashMap<Integer, ArrayList<LevelInfo>> parts;
    ArrayList<View> views;

//    private void createViews() {
//        int partInfo = -1;
//        LinearLayout ll = null;
//        int colN = 0;
//        boolean available = false;
//        double c = 0;
//        int a = 0;
//
//        for (int part = 1; part<=GameActivity.parts.size();part++) {
//
//            if (part == 1)
//                available = true;
//            else
//                available = (c/a>=GameActivity.PERCENT_COMPLETED);
//            int need = (int) Math.ceil(GameActivity.PERCENT_COMPLETED * a - c);
//
//            ArrayList<LevelInfo> levels = GameActivity.parts.get(part);
//            for (int i = 0; i < levels.size(); i++) {
//                if (levels.get(i).isCompleted())
//                    c++;
//                a++;
//            }
//
//            //Выводим номер части
//            TextView tv = new TextView(context);
//            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//            int m = (int) context.getResources().getDimension(R.dimen.margin_part);
//            lp.setMargins(m, m, m, m);
//            tv.setLayoutParams(lp);
//            String tag = "part_tv:"+part;
//            tv.setTag(tag);
//            tv.setTextSize(context.getResources().getDimension(R.dimen.ba_tv_part_textsize));
//            if (available)
//                tv.setText(context.getResources().getString(R.string.part_label)+" "+part);
//            else
//                tv.setText(String.format("%s %d (Осталось %d)", context.getResources().getString(R.string.part_label), part, need));
//            //root.addView(tv);
//            views.add(tv);
//            colN = 0;
//            for (int level = 0; level<levels.size(); level += BlockFragment.COLUMN_NUMBER) {
//                LinearLayout l_root = (LinearLayout) lInflater.inflate(R.layout.bf_list_item, null, false);
//                for (int i = 0; i < BlockFragment.COLUMN_NUMBER; i++) {
//                    //Добавляем элементы
//                    LevelInfo l = parts.get(part).get(level + i);
//                    l.setAvailable(available);
//                    FrameLayout img_layout;
//
//                    img_layout = (FrameLayout) lInflater.inflate(R.layout.ba_level_image, l_root, false);
//                    ImageView img = (ImageView) img_layout.findViewById(R.id.part_img_level);
//                    if (!l.isAvailable())
//                        img_layout.setForeground(context.getResources().getDrawable(R.drawable.locked_image));
//                    else if (l.isCompleted()) {
//                        //Рисуем галочку
//                        img_layout.setForeground(context.getResources().getDrawable(R.drawable.completed_image));
//                    } else
//                        img_layout.setForeground(context.getResources().getDrawable(R.drawable.border_image));
//
//                    img.setTag(l);
//                    if (l.isAvailable())
//                        img.setOnClickListener(GameActivity.onLevelSelected);
//                    Picasso.with(context).load(String.format("file:///android_asset/%s", l.getPic())).into(img);
//                    l_root.addView(img_layout);
//                }
//                views.add(l_root);
//            }
//
//        }
//    }

    //Заполяет rows.
    private void init() {
        boolean available;
        double c = 0;
        int a = 0;
        int x;
        for (int part = 1; part<=GameActivity.parts.size();part++) {
            if (part == 1)
                available = true;
            else
                available = (c/a>=GameActivity.PERCENT_COMPLETED);
            int need = 0;
            if (!available)
                need = (int) Math.ceil(GameActivity.PERCENT_COMPLETED * a - c);
            needed.put(part, need);

            ArrayList<LevelInfo> levels = GameActivity.parts.get(part);
            for (int i = 0; i < levels.size(); i++) {
                if (levels.get(i).isCompleted())
                    c++;
                a++;
            }
            x = TV_MASK | (part&PART_MASK);
            rows.add(x);

            for (LevelInfo l: levels) {
                l.setAvailable(available);
            }

            for (int i = 0; i < levels.size(); i += BlockFragment.COLUMN_NUMBER) {
                LevelInfo l = levels.get(i);
                x = (part&PART_MASK) | ((l.getLevel() << 8)&LEVEL_MASK);
                rows.add(x);
            }
        }
    }

    //Считает пройденнные уровни и разблокированные
    private void checkChanges() {
        rows.clear();
        needed.clear();
        init();
    }

    public BlockFragmentAdapter(Context context, HashMap<Integer, ArrayList<LevelInfo>> parts) {
        this.parts = parts;
        rows = new ArrayList<>();
        needed = new HashMap<>();
        views = new ArrayList<>();
        this.context = context;
        lInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        createViews();
        init();
    }

    @Override
    public int getCount() {
        return rows.size();
    }

    @Override
    public Object getItem(int position) {
        int x = rows.get(position);
        if ((x&TV_MASK) == 0) {
            int part = x&PART_MASK;
            int level = (x&LEVEL_MASK)>>8;
            return parts.get(part).get(level);
        }
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View view, ViewGroup root) {
//        return views.get(position);
        int x = rows.get(position);
        //Если рисуем элемент
        if ((x&TV_MASK) == 0) {
            int part = x&PART_MASK;
            int level = (x&LEVEL_MASK)>>8;
            LinearLayout l_root;
            if (view != null && view instanceof LinearLayout)
                l_root = (LinearLayout) view;
            else {
                l_root = (LinearLayout) lInflater.inflate(R.layout.bf_list_item, root, false);
            }
            AbsListView.LayoutParams lp;
            lp = new AbsListView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            l_root.setLayoutParams(lp);
            for (int i = 0; i < BlockFragment.COLUMN_NUMBER; i++) {
                //Добавляем элементы
                LevelInfo l = parts.get(part).get(level+i);
                FrameLayout img_layout;

                ImageView img;
                if (l_root.getChildCount() == BlockFragment.COLUMN_NUMBER) {
                    View v = ((ViewGroup)l_root.getChildAt(i)).getChildAt(0);
                    img_layout = (FrameLayout) v.getParent();
                    img = (ImageView) v;
                } else {
                    img_layout = (FrameLayout) lInflater.inflate(R.layout.ba_level_image, l_root, false);
                    img = (ImageView) img_layout.findViewById(R.id.part_img_level);
                    img_layout.getLayoutParams().width = BlockFragment.blockSize;
                    img_layout.getLayoutParams().height = BlockFragment.blockSize;
                    l_root.addView(img_layout);
                }
                img.setTag(l);
                if (!l.isAvailable())
                    img_layout.setForeground(context.getResources().getDrawable(R.drawable.locked_image));
                else if (l.isCompleted()) {
                    //Рисуем галочку
                    img_layout.setForeground(context.getResources().getDrawable(R.drawable.completed_image));
                } else
                    img_layout.setForeground(context.getResources().getDrawable(R.drawable.border_image));
                if (l.isAvailable())
                    img.setOnClickListener(GameActivity.onLevelSelected);
                else
                    img.setOnClickListener(null);
                Picasso.with(context).load(String.format("file:///android_asset/%s", l.getPic())).into(img);
            }
	        l_root.setClickable(false);
            return l_root;
        } else { //Если рисуем номер части
            int part = x&PART_MASK;
            TextView tv = new TextView(context);
            AbsListView.LayoutParams lp = new AbsListView.LayoutParams(AbsListView.LayoutParams.WRAP_CONTENT, AbsListView.LayoutParams.WRAP_CONTENT);
            int m = (int) context.getResources().getDimension(R.dimen.margin_part);
//            lp.setMargins(m, m, m, m);
            tv.setLayoutParams(lp);
            String tag = "part_tv:"+part;
            tv.setTag(tag);
            tv.setTextSize(context.getResources().getDimension(R.dimen.ba_tv_part_textsize));
            int need_v = needed.get(part);
            if (need_v == 0)
                tv.setText(context.getResources().getString(R.string.part_label)+" "+part);
            else
                tv.setText(String.format("%s %d (Осталось %d)", context.getResources().getString(R.string.part_label), part, need_v));
            tv.setClickable(false);
	        return tv;
        }
    }
}
