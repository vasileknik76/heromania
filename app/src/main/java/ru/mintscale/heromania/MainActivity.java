package ru.mintscale.heromania;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private static final String LOG_TAG = "MainActivity";
    private DBMainHelper dbHelper;


    //Сохраняет настройки в первый раз.
    private void fillSettings(SharedPreferences p) {
        if (p.contains(SettingsActivity.PARAM_LANG)) {
            return;
        }
        SharedPreferences.Editor e = p.edit();
        Locale l = Locale.getDefault();
        e.putString(SettingsActivity.PARAM_LANG, l.getLanguage());
        e.putInt(SettingsActivity.PARAM_COINS, SettingsActivity.DEFAULT_COINS);
        e.putInt(SettingsActivity.PARAM_BOOST_COIN, SettingsActivity.DEFAULT_BOOST_COUNT);
        e.putInt(SettingsActivity.PARAM_BOOST_LIFE, SettingsActivity.DEFAULT_BOOST_COUNT);
        e.putInt(SettingsActivity.PARAM_BOOST_TIME, SettingsActivity.DEFAULT_BOOST_COUNT);
        e.apply();
        e.commit();
    }

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		Log.d(LOG_TAG, "config changed");
	}

	@Override
    protected void onResume() {
        super.onResume();
        fillSettings(PreferenceManager.getDefaultSharedPreferences(this));
        if (SettingsActivity.isLanguageChanged()) {
            SettingsActivity.setLanguageChanged(false);
            recreate();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Нужный язык
        Locale locale = new Locale(PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsActivity.PARAM_LANG, SettingsActivity.DEFAULT_LANG));
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getResources().updateConfiguration(config, getResources().getDisplayMetrics());

        setContentView(R.layout.activity_main);
        SettingsActivity.init(getApplicationContext());
		//Устанавливаем обработчики кнопок
        ((Button)findViewById(R.id.buttonSettings)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), SettingsActivity.class);
                startActivity(i);
            }
        });
        ((Button)findViewById(R.id.buttonGame)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), GameActivity.class);
                startActivity(i);
//                overridePendingTransition(R.anim.slide_in_right, android.R.anim.fade_out);
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
            }
        });

        findViewById(R.id.buttonQuiz).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), QuizActivity.class);
                startActivity(i);
//                overridePendingTransition(R.anim.slide_in_right, android.R.anim.fade_out);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
            }
        });


    }
}
