package ru.mintscale.heromania;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

/**
 * Created by nik on 09.06.2016.
 */
public class BlockPagerAdapter extends FragmentPagerAdapter {
    private ArrayList<LevelInfo> levels; //На самом деле хранится в GameActivity
    public BlockPagerAdapter(FragmentManager fm) {
        super(fm);
        this.levels = levels;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Fragment getItem(int pos) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        return new BlockFragment();

        //return PlaceholderFragment.newInstance(pos);
    }

    @Override
    public int getCount() {
        // Show 24 total pages.
//        Cursor c = LevelActivity.db.rawQuery("SELECT * FROM part_to_level_guess WHERE partNumber="+LevelActivity.partNumber, null);
//        int cnt = c.getCount();
//        c.close();
        return 1;
        //return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return null;
    }
}
