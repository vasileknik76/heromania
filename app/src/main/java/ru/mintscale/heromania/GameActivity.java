package ru.mintscale.heromania;

import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class GameActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener {
    //И block и level

    public static final String PARAM_POSITION = "pos";
    public static final String PARAM_PART = "part";
    public static final String HINT_KEY = "hint";
    public static final int HINT_LETTER = 1; //Подсказка выбранная буква
    public static final int HINT_REMOVE_WASTE_LETTERS = 2; //Подсказка убрать лишние буквы
    public static final int HINT_HALF_WORD = 3; //подсказка отгадать половину букв слова
    public static final int HINT_DIALOG_OK = 1;
    public static final int HINT_DIALOG_CANCEL = 2;
    public static final int HINT_DIALOG_REQUEST = 1;
    public static final Character SYMBOL_NONE = '$';
    public static int money;

    //Процент прохождения после которого открывается очередная часть
    public static final double PERCENT_COMPLETED = 1;
    private static final String LOG_TAG = "GameActivity";
    public static final int SCROLL_TIMEOUT = 600;
    public static DBMainHelper dbMainHelper;
    private static boolean isPageTracking;
    private static boolean isPageChanged;
    private int blockSize;
    LayoutInflater inflater;
    LinearLayout root;
    public static HashMap<Integer, ArrayList<LevelInfo>> parts;

    private TextView info;
    private TextView coins;

	//Handler для обновления заголовка монет.
    public static Handler titleUpdater;

	//Handler для обновления количества монет
	public static Handler coinsUpdater;
    private PagerAdapter pagerAdapter;
    public static ViewPager mViewPager;
    public static View.OnClickListener onLevelSelected;
    public static BlockFragment blockFragment;
    public static FragmentManager fm;
    private int currentPart = -1;

    public static boolean isPageTracking() {
        return isPageTracking;
    }

    public static void setIsPageTracking(boolean isPageTracking) {
        GameActivity.isPageTracking = isPageTracking;
        setIsPageChanged(false);
    }

    public static boolean isPageChanged() {
        return isPageChanged;
    }

    public static void setIsPageChanged(boolean isPageChanged) {
        GameActivity.isPageChanged = isPageChanged;
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
	    Log.d(LOG_TAG, "onCreate");
        setContentView(R.layout.activity_game);
	    //Инициализация базы и настроек
        SettingsActivity.init(getApplicationContext());
        try {
            dbMainHelper = new DBMainHelper(this);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
	    //Читаем уровни
        parts = dbMainHelper.loadGuessLevels();

        money = SettingsActivity.getInt(SettingsActivity.PARAM_COINS);

        coinsUpdater = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                coins.setText(String.valueOf(money));
            }
        };

        titleUpdater = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                if (msg.obj instanceof Integer) {
                    int n = (int) msg.obj;
                    String text = String.format("%s %d",
                            getApplicationContext().getResources().getString(R.string.level_label), n);
                    info.setText(text);
                } else {
                    String s = (String) msg.obj;
                    info.setText(s);
                }

            }
        };
        // Создаем фрагмент
        blockFragment = BlockFragment.newInstance();
        fm = getSupportFragmentManager();
        fm.beginTransaction()
                //.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                .add(R.id.GA_frame_root, blockFragment, "block")
                .commit();

        info = (TextView) findViewById(R.id.text_level);
        info.setText(getResources().getString(R.string.level_choice));
        coins = (TextView) findViewById(R.id.text_coins);
        coinsUpdater.sendEmptyMessage(0);
        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.view_pager);
        mViewPager.setPageTransformer(true, new DepthPageTransformer());
        mViewPager.addOnPageChangeListener(this);
//        mViewPager.setScrollDuration(SCROLL_TIMEOUT);
        mViewPager.setAdapter(pagerAdapter);

        //Обработчик нажатия на кнопку выбора уровння
        onLevelSelected = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //getSupportFragmentManager().beginTransaction().remove(blockFragment).commit();

                LevelInfo li = (LevelInfo) view.getTag();
                if (currentPart == li.getPart()) {
                    mViewPager.setCurrentItem(li.getLevel(), false);
                    LevelFragment fragment = (LevelFragment) getSupportFragmentManager().findFragmentByTag("android:switcher:"+R.id.view_pager+":"+li.getLevel());
                    fragment.onShow();
                    getSupportFragmentManager().beginTransaction()
                            .setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_left)
                            .hide(blockFragment).commit();
                    return;
                }
                getSupportFragmentManager().beginTransaction()
                        .setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_left)
                        .hide(blockFragment).commit();
                currentPart = li.getPart();

                //Чистим старый адаптер если он есть
                if (pagerAdapter != null) {
                    FragmentManager fragmentManager = ((LevelPagerAdapter)pagerAdapter).getFragmentManager();
                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                    List<Fragment> fragments = fragmentManager.getFragments();
                    for (Fragment f : fragments) {
                        if (f == null)
                            continue;
                        if (f == blockFragment)
                            continue;
                        transaction.remove(f);
                    }
                    transaction.commit();
//                    mViewPager.removeAllViews();
//                    mViewPager.setAdapter(null);
                }

                pagerAdapter = new LevelPagerAdapter(getSupportFragmentManager(), parts.get(li.getPart()));
                Log.d(LOG_TAG, "haha "+mViewPager+":"+pagerAdapter);
                mViewPager.setOffscreenPageLimit(parts.get(li.getPart()).size()); //Размер выбранной части
//                mViewPager.setScrollDuration(SCROLL_TIMEOUT);
                mViewPager.setAdapter(pagerAdapter);
                mViewPager.setCurrentItem(li.getLevel());
                LevelFragment fragment = (LevelFragment) getSupportFragmentManager().findFragmentByTag("android:switcher:"+R.id.view_pager+":"+li.getLevel());
                fragment.onShow();
                //onPageSelected(li.getLevel());
            }
        };
    }


    @Override
    protected void onPause() {
        super.onPause();
//        overridePendingTransition(android.R.anim.fade_in, R.anim.slide_in_left);
//        overridePendingTransition(R.anim.slide_in_right, android.R.anim.fade_out);
        if (blockFragment.isHidden()) {
            if (currentPart != -1) {
                dbMainHelper.saveGuessLevels(parts.get(currentPart), currentPart);
                Log.d(LOG_TAG, "onPause save part "+currentPart);
            }
        }
        Log.d(LOG_TAG, "save money!:)");
        SettingsActivity.set(SettingsActivity.PARAM_COINS, money);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

    @Override
    protected void onStop() {
        super.onStop();
//        destroyViews();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(LOG_TAG, "onStart");
//        readLevels();
//        createViews();
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        if (isPageTracking)
            setIsPageChanged(true);

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Fragment f = fm.findFragmentByTag("block");
            if (f.isHidden()) {
	            dbMainHelper.saveGuessLevels(parts.get(currentPart), currentPart);
                //fm.beginTransaction().add(R.id.frame_root, blockFragment, "block").commit();
                fm.beginTransaction()
                        .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_right)
                        .show(blockFragment).commit();
                Message msg = new Message();
                msg.obj = getResources().getString(R.string.level_choice);
                titleUpdater.sendMessage(msg);
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onPageSelected(int position) {
        if (isPageTracking())
            setIsPageChanged(true);
        Log.d(LOG_TAG, "onPageSelected: "+position);
        LevelFragment fragment = (LevelFragment) getSupportFragmentManager().findFragmentByTag("android:switcher:"+R.id.view_pager+":"+position);
        fragment.onShow();
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
